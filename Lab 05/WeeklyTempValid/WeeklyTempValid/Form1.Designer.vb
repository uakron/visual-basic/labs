﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormTempAvg
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnCalc = New System.Windows.Forms.Button()
        Me.BtnClear = New System.Windows.Forms.Button()
        Me.BtnExit = New System.Windows.Forms.Button()
        Me.TextWeek1 = New System.Windows.Forms.TextBox()
        Me.TextWeek2 = New System.Windows.Forms.TextBox()
        Me.TextWeek3 = New System.Windows.Forms.TextBox()
        Me.TextWeek4 = New System.Windows.Forms.TextBox()
        Me.TextWeek5 = New System.Windows.Forms.TextBox()
        Me.TextAverage = New System.Windows.Forms.TextBox()
        Me.LblWeek1 = New System.Windows.Forms.Label()
        Me.LblWeek2 = New System.Windows.Forms.Label()
        Me.LblWeek3 = New System.Windows.Forms.Label()
        Me.LblWeek4 = New System.Windows.Forms.Label()
        Me.LblWeek5 = New System.Windows.Forms.Label()
        Me.LblAverage = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.LblStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'BtnCalc
        '
        Me.BtnCalc.Location = New System.Drawing.Point(68, 295)
        Me.BtnCalc.Name = "BtnCalc"
        Me.BtnCalc.Size = New System.Drawing.Size(75, 23)
        Me.BtnCalc.TabIndex = 6
        Me.BtnCalc.Text = "Calculate"
        Me.BtnCalc.UseVisualStyleBackColor = True
        '
        'BtnClear
        '
        Me.BtnClear.Location = New System.Drawing.Point(295, 295)
        Me.BtnClear.Name = "BtnClear"
        Me.BtnClear.Size = New System.Drawing.Size(75, 23)
        Me.BtnClear.TabIndex = 7
        Me.BtnClear.Text = "Clear"
        Me.BtnClear.UseVisualStyleBackColor = True
        '
        'BtnExit
        '
        Me.BtnExit.Location = New System.Drawing.Point(517, 295)
        Me.BtnExit.Name = "BtnExit"
        Me.BtnExit.Size = New System.Drawing.Size(75, 23)
        Me.BtnExit.TabIndex = 8
        Me.BtnExit.Text = "Exit"
        Me.BtnExit.UseVisualStyleBackColor = True
        '
        'TextWeek1
        '
        Me.TextWeek1.Location = New System.Drawing.Point(173, 71)
        Me.TextWeek1.Name = "TextWeek1"
        Me.TextWeek1.Size = New System.Drawing.Size(100, 20)
        Me.TextWeek1.TabIndex = 0
        '
        'TextWeek2
        '
        Me.TextWeek2.Location = New System.Drawing.Point(173, 131)
        Me.TextWeek2.Name = "TextWeek2"
        Me.TextWeek2.Size = New System.Drawing.Size(100, 20)
        Me.TextWeek2.TabIndex = 1
        '
        'TextWeek3
        '
        Me.TextWeek3.Location = New System.Drawing.Point(173, 198)
        Me.TextWeek3.Name = "TextWeek3"
        Me.TextWeek3.Size = New System.Drawing.Size(100, 20)
        Me.TextWeek3.TabIndex = 2
        '
        'TextWeek4
        '
        Me.TextWeek4.Location = New System.Drawing.Point(492, 71)
        Me.TextWeek4.Name = "TextWeek4"
        Me.TextWeek4.Size = New System.Drawing.Size(100, 20)
        Me.TextWeek4.TabIndex = 3
        '
        'TextWeek5
        '
        Me.TextWeek5.Location = New System.Drawing.Point(492, 131)
        Me.TextWeek5.Name = "TextWeek5"
        Me.TextWeek5.Size = New System.Drawing.Size(100, 20)
        Me.TextWeek5.TabIndex = 4
        '
        'TextAverage
        '
        Me.TextAverage.Enabled = False
        Me.TextAverage.Location = New System.Drawing.Point(492, 198)
        Me.TextAverage.Name = "TextAverage"
        Me.TextAverage.Size = New System.Drawing.Size(100, 20)
        Me.TextAverage.TabIndex = 5
        '
        'LblWeek1
        '
        Me.LblWeek1.AutoSize = True
        Me.LblWeek1.Location = New System.Drawing.Point(65, 78)
        Me.LblWeek1.Name = "LblWeek1"
        Me.LblWeek1.Size = New System.Drawing.Size(45, 13)
        Me.LblWeek1.TabIndex = 9
        Me.LblWeek1.Text = "Week 1"
        '
        'LblWeek2
        '
        Me.LblWeek2.AutoSize = True
        Me.LblWeek2.Location = New System.Drawing.Point(65, 138)
        Me.LblWeek2.Name = "LblWeek2"
        Me.LblWeek2.Size = New System.Drawing.Size(45, 13)
        Me.LblWeek2.TabIndex = 10
        Me.LblWeek2.Text = "Week 2"
        '
        'LblWeek3
        '
        Me.LblWeek3.AutoSize = True
        Me.LblWeek3.Location = New System.Drawing.Point(65, 205)
        Me.LblWeek3.Name = "LblWeek3"
        Me.LblWeek3.Size = New System.Drawing.Size(45, 13)
        Me.LblWeek3.TabIndex = 11
        Me.LblWeek3.Text = "Week 3"
        '
        'LblWeek4
        '
        Me.LblWeek4.AutoSize = True
        Me.LblWeek4.Location = New System.Drawing.Point(376, 78)
        Me.LblWeek4.Name = "LblWeek4"
        Me.LblWeek4.Size = New System.Drawing.Size(45, 13)
        Me.LblWeek4.TabIndex = 12
        Me.LblWeek4.Text = "Week 4"
        '
        'LblWeek5
        '
        Me.LblWeek5.AutoSize = True
        Me.LblWeek5.Location = New System.Drawing.Point(376, 138)
        Me.LblWeek5.Name = "LblWeek5"
        Me.LblWeek5.Size = New System.Drawing.Size(45, 13)
        Me.LblWeek5.TabIndex = 13
        Me.LblWeek5.Text = "Week 5"
        '
        'LblAverage
        '
        Me.LblAverage.AutoSize = True
        Me.LblAverage.Location = New System.Drawing.Point(376, 205)
        Me.LblAverage.Name = "LblAverage"
        Me.LblAverage.Size = New System.Drawing.Size(50, 13)
        Me.LblAverage.TabIndex = 14
        Me.LblAverage.Text = "Average:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LblStatus})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 362)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(665, 22)
        Me.StatusStrip1.TabIndex = 15
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'LblStatus
        '
        Me.LblStatus.Name = "LblStatus"
        Me.LblStatus.Size = New System.Drawing.Size(0, 17)
        '
        'FormTempAvg
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(665, 384)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.LblAverage)
        Me.Controls.Add(Me.LblWeek5)
        Me.Controls.Add(Me.LblWeek4)
        Me.Controls.Add(Me.LblWeek3)
        Me.Controls.Add(Me.LblWeek2)
        Me.Controls.Add(Me.LblWeek1)
        Me.Controls.Add(Me.TextAverage)
        Me.Controls.Add(Me.TextWeek5)
        Me.Controls.Add(Me.TextWeek4)
        Me.Controls.Add(Me.TextWeek3)
        Me.Controls.Add(Me.TextWeek2)
        Me.Controls.Add(Me.TextWeek1)
        Me.Controls.Add(Me.BtnExit)
        Me.Controls.Add(Me.BtnClear)
        Me.Controls.Add(Me.BtnCalc)
        Me.Name = "FormTempAvg"
        Me.Text = "Weekly Temperature Average"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BtnCalc As Button
    Friend WithEvents BtnClear As Button
    Friend WithEvents BtnExit As Button
    Friend WithEvents TextWeek1 As TextBox
    Friend WithEvents TextWeek2 As TextBox
    Friend WithEvents TextWeek3 As TextBox
    Friend WithEvents TextWeek4 As TextBox
    Friend WithEvents TextWeek5 As TextBox
    Friend WithEvents TextAverage As TextBox
    Friend WithEvents LblWeek1 As Label
    Friend WithEvents LblWeek2 As Label
    Friend WithEvents LblWeek3 As Label
    Friend WithEvents LblWeek4 As Label
    Friend WithEvents LblWeek5 As Label
    Friend WithEvents LblAverage As Label
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents LblStatus As ToolStripStatusLabel
End Class
