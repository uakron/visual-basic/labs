﻿Public Class FormTempAvg
    Private Sub BtnExit_Click(sender As Object, e As EventArgs) Handles BtnExit.Click
        Me.Close()
    End Sub

    Private Sub BtnClear_Click(sender As Object, e As EventArgs) Handles BtnClear.Click
        TextWeek1.Clear()
        TextWeek2.Clear()
        TextWeek3.Clear()
        TextWeek4.Clear()
        TextWeek5.Clear()
        TextAverage.Clear()
        LblStatus.Text = ""
    End Sub

    Private Sub BtnCalc_Click(sender As Object, e As EventArgs) Handles BtnCalc.Click
        Dim tempWeek1, tempWeek2, tempWeek3, tempWeek4, tempWeek5 As Double
        Dim tempAvg As Double

        If Not Double.TryParse(TextWeek1.Text, tempWeek1) Then
            LblStatus.Text = "Temperature 1 must be numeric."
            TextWeek1.Focus()
            TextWeek1.SelectAll()
            Exit Sub
        End If

        If tempWeek1 < -50 Or tempWeek1 > 130 Then
            LblStatus.Text = "Temperature 1 must be between -50 and 130."
            TextWeek1.Focus()
            TextWeek1.SelectAll()
            Exit Sub
        End If

        If Not Double.TryParse(TextWeek2.Text, tempWeek2) Then
            LblStatus.Text = "Temperature 2 must be numeric."
            TextWeek2.Focus()
            TextWeek2.SelectAll()
            Exit Sub
        End If

        If tempWeek2 < -50 Or tempWeek2 > 130 Then
            LblStatus.Text = "Temperature 2 must be between -50 and 130."
            TextWeek2.Focus()
            TextWeek2.SelectAll()
            Exit Sub
        End If

        If Not Double.TryParse(TextWeek3.Text, tempWeek3) Then
            LblStatus.Text = "Temperature 3 must be numeric."
            TextWeek3.Focus()
            TextWeek3.SelectAll()
            Exit Sub
        End If

        If tempWeek3 < -50 Or tempWeek3 > 130 Then
            LblStatus.Text = "Temperature 3 must be between -50 and 130."
            TextWeek3.Focus()
            TextWeek3.SelectAll()
            Exit Sub
        End If

        If Not Double.TryParse(TextWeek4.Text, tempWeek4) Then
            LblStatus.Text = "Temperature 4 must be numeric."
            TextWeek4.Focus()
            TextWeek4.SelectAll()
            Exit Sub
        End If

        If tempWeek4 < -50 Or tempWeek4 > 130 Then
            LblStatus.Text = "Temperature 4 must be between -50 and 130."
            TextWeek4.Focus()
            TextWeek4.SelectAll()
            Exit Sub
        End If

        If Not Double.TryParse(TextWeek5.Text, tempWeek5) Then
            LblStatus.Text = "Temperature 5 must be numeric."
            TextWeek5.Focus()
            TextWeek5.SelectAll()
            Exit Sub
        End If

        If tempWeek5 < -50 Or tempWeek5 > 130 Then
            LblStatus.Text = "Temperature 5 must be between -50 and 130."
            TextWeek5.Focus()
            TextWeek5.SelectAll()
            Exit Sub
        End If

        LblStatus.Text = ""

        tempAvg = (tempWeek1 + tempWeek2 + tempWeek3 + tempWeek4 + tempWeek5) / 5

        TextAverage.Text = tempAvg
    End Sub
End Class
