﻿Public Class ConferenceOptions
    Dim dblRegistration As Double = 895
    Dim dblDinnerKeynote As Double = 30
    Dim dblCommerce As Double = 295
    Dim dblFutureWeb As Double = 295
    Dim dblAdvancedVB As Double = 395
    Dim dblNetworkSec As Double = 395
    Public dblTotal As Double = 0

    Private Sub BtnClose_Click(sender As Object, e As EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub BtnReset_Click(sender As Object, e As EventArgs) Handles BtnReset.Click
        CheckRegistration.Checked = False
        CheckDinnerKeynote.Checked = False
        ListBoxWorkshops.ClearSelected()
        dblTotal = 0
    End Sub

    Private Sub ConferenceOptions_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If CheckRegistration.Checked = True Then
            dblTotal += dblRegistration
        End If
        If CheckDinnerKeynote.Checked = True Then
            dblTotal += dblDinnerKeynote
        End If
        If ListBoxWorkshops.SelectedIndex = 0 Then
            dblTotal += dblCommerce
        End If
        If ListBoxWorkshops.SelectedIndex = 1 Then
            dblTotal += dblFutureWeb
        End If
        If ListBoxWorkshops.SelectedIndex = 2 Then
            dblTotal += dblAdvancedVB
        End If
        If ListBoxWorkshops.SelectedIndex = 3 Then
            dblTotal += dblNetworkSec
        End If
    End Sub

    Private Sub ListBoxWorkshops_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxWorkshops.SelectedIndexChanged
        If CheckRegistration.Checked = False Then
            MessageBox.Show("Please select the conference registration option first.")
        End If
    End Sub
End Class