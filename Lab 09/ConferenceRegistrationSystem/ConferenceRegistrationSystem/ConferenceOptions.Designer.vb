﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConferenceOptions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblConference = New System.Windows.Forms.Label()
        Me.CheckRegistration = New System.Windows.Forms.CheckBox()
        Me.CheckDinnerKeynote = New System.Windows.Forms.CheckBox()
        Me.ListBoxWorkshops = New System.Windows.Forms.ListBox()
        Me.LblSelect = New System.Windows.Forms.Label()
        Me.LblWorkshops = New System.Windows.Forms.Label()
        Me.BtnReset = New System.Windows.Forms.Button()
        Me.BtnClose = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LblConference
        '
        Me.LblConference.AutoSize = True
        Me.LblConference.Location = New System.Drawing.Point(13, 13)
        Me.LblConference.Name = "LblConference"
        Me.LblConference.Size = New System.Drawing.Size(92, 20)
        Me.LblConference.TabIndex = 0
        Me.LblConference.Text = "Conference"
        '
        'CheckRegistration
        '
        Me.CheckRegistration.AutoSize = True
        Me.CheckRegistration.Location = New System.Drawing.Point(31, 86)
        Me.CheckRegistration.Name = "CheckRegistration"
        Me.CheckRegistration.Size = New System.Drawing.Size(258, 24)
        Me.CheckRegistration.TabIndex = 1
        Me.CheckRegistration.Text = "Conference Registration ($895)"
        Me.CheckRegistration.UseVisualStyleBackColor = True
        '
        'CheckDinnerKeynote
        '
        Me.CheckDinnerKeynote.AutoSize = True
        Me.CheckDinnerKeynote.Location = New System.Drawing.Point(31, 146)
        Me.CheckDinnerKeynote.Name = "CheckDinnerKeynote"
        Me.CheckDinnerKeynote.Size = New System.Drawing.Size(321, 24)
        Me.CheckDinnerKeynote.TabIndex = 2
        Me.CheckDinnerKeynote.Text = "Opening Night Dinner and Keynote ($30)"
        Me.CheckDinnerKeynote.UseVisualStyleBackColor = True
        '
        'ListBoxWorkshops
        '
        Me.ListBoxWorkshops.FormattingEnabled = True
        Me.ListBoxWorkshops.ItemHeight = 20
        Me.ListBoxWorkshops.Items.AddRange(New Object() {"Into to E-commerce ($295)", "The Future of the Web ($295)", "Advanced Visual Basic ($395)", "Network Security ($395)"})
        Me.ListBoxWorkshops.Location = New System.Drawing.Point(408, 86)
        Me.ListBoxWorkshops.Name = "ListBoxWorkshops"
        Me.ListBoxWorkshops.Size = New System.Drawing.Size(230, 84)
        Me.ListBoxWorkshops.TabIndex = 3
        '
        'LblSelect
        '
        Me.LblSelect.AutoSize = True
        Me.LblSelect.Location = New System.Drawing.Point(408, 60)
        Me.LblSelect.Name = "LblSelect"
        Me.LblSelect.Size = New System.Drawing.Size(88, 20)
        Me.LblSelect.TabIndex = 4
        Me.LblSelect.Text = "Select One"
        '
        'LblWorkshops
        '
        Me.LblWorkshops.AutoSize = True
        Me.LblWorkshops.Location = New System.Drawing.Point(369, 9)
        Me.LblWorkshops.Name = "LblWorkshops"
        Me.LblWorkshops.Size = New System.Drawing.Size(197, 20)
        Me.LblWorkshops.TabIndex = 5
        Me.LblWorkshops.Text = "Preconference Workshops"
        '
        'BtnReset
        '
        Me.BtnReset.Location = New System.Drawing.Point(344, 202)
        Me.BtnReset.Name = "BtnReset"
        Me.BtnReset.Size = New System.Drawing.Size(124, 57)
        Me.BtnReset.TabIndex = 6
        Me.BtnReset.Text = "Reset"
        Me.BtnReset.UseVisualStyleBackColor = True
        '
        'BtnClose
        '
        Me.BtnClose.Location = New System.Drawing.Point(496, 202)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Size = New System.Drawing.Size(124, 57)
        Me.BtnClose.TabIndex = 7
        Me.BtnClose.Text = "Close"
        Me.BtnClose.UseVisualStyleBackColor = True
        '
        'ConferenceOptions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(662, 289)
        Me.Controls.Add(Me.BtnClose)
        Me.Controls.Add(Me.BtnReset)
        Me.Controls.Add(Me.LblWorkshops)
        Me.Controls.Add(Me.LblSelect)
        Me.Controls.Add(Me.ListBoxWorkshops)
        Me.Controls.Add(Me.CheckDinnerKeynote)
        Me.Controls.Add(Me.CheckRegistration)
        Me.Controls.Add(Me.LblConference)
        Me.Name = "ConferenceOptions"
        Me.Text = "Conference Options"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LblConference As Label
    Friend WithEvents CheckRegistration As CheckBox
    Friend WithEvents CheckDinnerKeynote As CheckBox
    Friend WithEvents ListBoxWorkshops As ListBox
    Friend WithEvents LblSelect As Label
    Friend WithEvents LblWorkshops As Label
    Friend WithEvents BtnReset As Button
    Friend WithEvents BtnClose As Button
End Class
