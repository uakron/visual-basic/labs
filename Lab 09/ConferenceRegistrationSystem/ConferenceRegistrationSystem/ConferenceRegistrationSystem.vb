﻿Public Class ConferenceRegistrationSystem
    Private Sub BtnExit_Click(sender As Object, e As EventArgs) Handles BtnExit.Click
        Me.Close()
    End Sub

    Private Sub BtnReset_Click(sender As Object, e As EventArgs) Handles BtnReset.Click
        TextName.Clear()
        TextCompany.Clear()
        TextAddress.Clear()
        TextCity.Clear()
        TextPhone.Clear()
        TextEmail.Clear()
        TextState.Clear()
        TextZip.Clear()
    End Sub

    Private Sub BtnOptions_Click(sender As Object, e As EventArgs) Handles BtnOptions.Click
        ConferenceOptions.ShowDialog()
        ConferenceOptions.dblTotal = 0
    End Sub

    Private Sub ConferenceRegistrationSystem_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        TextTotal.Text = ConferenceOptions.dblTotal.ToString("c")
    End Sub
End Class