﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConferenceRegistrationSystem
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblRegistrant = New System.Windows.Forms.Label()
        Me.LblName = New System.Windows.Forms.Label()
        Me.LblCompany = New System.Windows.Forms.Label()
        Me.LblAddress = New System.Windows.Forms.Label()
        Me.LblCity = New System.Windows.Forms.Label()
        Me.LblPhone = New System.Windows.Forms.Label()
        Me.LblEmail = New System.Windows.Forms.Label()
        Me.LblState = New System.Windows.Forms.Label()
        Me.LblZip = New System.Windows.Forms.Label()
        Me.LblTotal = New System.Windows.Forms.Label()
        Me.TextTotal = New System.Windows.Forms.TextBox()
        Me.BtnOptions = New System.Windows.Forms.Button()
        Me.BtnReset = New System.Windows.Forms.Button()
        Me.BtnExit = New System.Windows.Forms.Button()
        Me.TextName = New System.Windows.Forms.TextBox()
        Me.TextCompany = New System.Windows.Forms.TextBox()
        Me.TextAddress = New System.Windows.Forms.TextBox()
        Me.TextCity = New System.Windows.Forms.TextBox()
        Me.TextPhone = New System.Windows.Forms.TextBox()
        Me.TextEmail = New System.Windows.Forms.TextBox()
        Me.TextState = New System.Windows.Forms.TextBox()
        Me.TextZip = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'LblRegistrant
        '
        Me.LblRegistrant.AutoSize = True
        Me.LblRegistrant.Location = New System.Drawing.Point(13, 13)
        Me.LblRegistrant.Name = "LblRegistrant"
        Me.LblRegistrant.Size = New System.Drawing.Size(83, 20)
        Me.LblRegistrant.TabIndex = 0
        Me.LblRegistrant.Text = "Registrant"
        '
        'LblName
        '
        Me.LblName.AutoSize = True
        Me.LblName.Location = New System.Drawing.Point(38, 80)
        Me.LblName.Name = "LblName"
        Me.LblName.Size = New System.Drawing.Size(51, 20)
        Me.LblName.TabIndex = 1
        Me.LblName.Text = "Name"
        '
        'LblCompany
        '
        Me.LblCompany.AutoSize = True
        Me.LblCompany.Location = New System.Drawing.Point(38, 116)
        Me.LblCompany.Name = "LblCompany"
        Me.LblCompany.Size = New System.Drawing.Size(76, 20)
        Me.LblCompany.TabIndex = 2
        Me.LblCompany.Text = "Company"
        '
        'LblAddress
        '
        Me.LblAddress.AutoSize = True
        Me.LblAddress.Location = New System.Drawing.Point(38, 153)
        Me.LblAddress.Name = "LblAddress"
        Me.LblAddress.Size = New System.Drawing.Size(68, 20)
        Me.LblAddress.TabIndex = 3
        Me.LblAddress.Text = "Address"
        '
        'LblCity
        '
        Me.LblCity.AutoSize = True
        Me.LblCity.Location = New System.Drawing.Point(38, 189)
        Me.LblCity.Name = "LblCity"
        Me.LblCity.Size = New System.Drawing.Size(35, 20)
        Me.LblCity.TabIndex = 4
        Me.LblCity.Text = "City"
        '
        'LblPhone
        '
        Me.LblPhone.AutoSize = True
        Me.LblPhone.Location = New System.Drawing.Point(333, 80)
        Me.LblPhone.Name = "LblPhone"
        Me.LblPhone.Size = New System.Drawing.Size(55, 20)
        Me.LblPhone.TabIndex = 5
        Me.LblPhone.Text = "Phone"
        '
        'LblEmail
        '
        Me.LblEmail.AutoSize = True
        Me.LblEmail.Location = New System.Drawing.Point(333, 116)
        Me.LblEmail.Name = "LblEmail"
        Me.LblEmail.Size = New System.Drawing.Size(48, 20)
        Me.LblEmail.TabIndex = 6
        Me.LblEmail.Text = "Email"
        '
        'LblState
        '
        Me.LblState.AutoSize = True
        Me.LblState.Location = New System.Drawing.Point(333, 153)
        Me.LblState.Name = "LblState"
        Me.LblState.Size = New System.Drawing.Size(48, 20)
        Me.LblState.TabIndex = 7
        Me.LblState.Text = "State"
        '
        'LblZip
        '
        Me.LblZip.AutoSize = True
        Me.LblZip.Location = New System.Drawing.Point(333, 189)
        Me.LblZip.Name = "LblZip"
        Me.LblZip.Size = New System.Drawing.Size(31, 20)
        Me.LblZip.TabIndex = 8
        Me.LblZip.Text = "Zip"
        '
        'LblTotal
        '
        Me.LblTotal.AutoSize = True
        Me.LblTotal.Location = New System.Drawing.Point(372, 281)
        Me.LblTotal.Name = "LblTotal"
        Me.LblTotal.Size = New System.Drawing.Size(44, 20)
        Me.LblTotal.TabIndex = 9
        Me.LblTotal.Text = "Total"
        '
        'TextTotal
        '
        Me.TextTotal.Enabled = False
        Me.TextTotal.Location = New System.Drawing.Point(440, 278)
        Me.TextTotal.Name = "TextTotal"
        Me.TextTotal.Size = New System.Drawing.Size(100, 26)
        Me.TextTotal.TabIndex = 10
        '
        'BtnOptions
        '
        Me.BtnOptions.Location = New System.Drawing.Point(28, 360)
        Me.BtnOptions.Name = "BtnOptions"
        Me.BtnOptions.Size = New System.Drawing.Size(172, 58)
        Me.BtnOptions.TabIndex = 11
        Me.BtnOptions.Text = "Select Conference Options"
        Me.BtnOptions.UseVisualStyleBackColor = True
        '
        'BtnReset
        '
        Me.BtnReset.Location = New System.Drawing.Point(239, 360)
        Me.BtnReset.Name = "BtnReset"
        Me.BtnReset.Size = New System.Drawing.Size(159, 58)
        Me.BtnReset.TabIndex = 12
        Me.BtnReset.Text = "Reset"
        Me.BtnReset.UseVisualStyleBackColor = True
        '
        'BtnExit
        '
        Me.BtnExit.Location = New System.Drawing.Point(429, 360)
        Me.BtnExit.Name = "BtnExit"
        Me.BtnExit.Size = New System.Drawing.Size(167, 58)
        Me.BtnExit.TabIndex = 13
        Me.BtnExit.Text = "Exit"
        Me.BtnExit.UseVisualStyleBackColor = True
        '
        'TextName
        '
        Me.TextName.Location = New System.Drawing.Point(152, 74)
        Me.TextName.Name = "TextName"
        Me.TextName.Size = New System.Drawing.Size(158, 26)
        Me.TextName.TabIndex = 14
        '
        'TextCompany
        '
        Me.TextCompany.Location = New System.Drawing.Point(152, 110)
        Me.TextCompany.Name = "TextCompany"
        Me.TextCompany.Size = New System.Drawing.Size(158, 26)
        Me.TextCompany.TabIndex = 15
        '
        'TextAddress
        '
        Me.TextAddress.Location = New System.Drawing.Point(152, 147)
        Me.TextAddress.Name = "TextAddress"
        Me.TextAddress.Size = New System.Drawing.Size(158, 26)
        Me.TextAddress.TabIndex = 16
        '
        'TextCity
        '
        Me.TextCity.Location = New System.Drawing.Point(152, 183)
        Me.TextCity.Name = "TextCity"
        Me.TextCity.Size = New System.Drawing.Size(158, 26)
        Me.TextCity.TabIndex = 17
        '
        'TextPhone
        '
        Me.TextPhone.Location = New System.Drawing.Point(414, 71)
        Me.TextPhone.Name = "TextPhone"
        Me.TextPhone.Size = New System.Drawing.Size(158, 26)
        Me.TextPhone.TabIndex = 18
        '
        'TextEmail
        '
        Me.TextEmail.Location = New System.Drawing.Point(414, 110)
        Me.TextEmail.Name = "TextEmail"
        Me.TextEmail.Size = New System.Drawing.Size(158, 26)
        Me.TextEmail.TabIndex = 19
        '
        'TextState
        '
        Me.TextState.Location = New System.Drawing.Point(414, 147)
        Me.TextState.Name = "TextState"
        Me.TextState.Size = New System.Drawing.Size(158, 26)
        Me.TextState.TabIndex = 20
        '
        'TextZip
        '
        Me.TextZip.Location = New System.Drawing.Point(414, 183)
        Me.TextZip.Name = "TextZip"
        Me.TextZip.Size = New System.Drawing.Size(158, 26)
        Me.TextZip.TabIndex = 21
        '
        'ConferenceRegistrationSystem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(630, 445)
        Me.Controls.Add(Me.TextZip)
        Me.Controls.Add(Me.TextState)
        Me.Controls.Add(Me.TextEmail)
        Me.Controls.Add(Me.TextPhone)
        Me.Controls.Add(Me.TextCity)
        Me.Controls.Add(Me.TextAddress)
        Me.Controls.Add(Me.TextCompany)
        Me.Controls.Add(Me.TextName)
        Me.Controls.Add(Me.BtnExit)
        Me.Controls.Add(Me.BtnReset)
        Me.Controls.Add(Me.BtnOptions)
        Me.Controls.Add(Me.TextTotal)
        Me.Controls.Add(Me.LblTotal)
        Me.Controls.Add(Me.LblZip)
        Me.Controls.Add(Me.LblState)
        Me.Controls.Add(Me.LblEmail)
        Me.Controls.Add(Me.LblPhone)
        Me.Controls.Add(Me.LblCity)
        Me.Controls.Add(Me.LblAddress)
        Me.Controls.Add(Me.LblCompany)
        Me.Controls.Add(Me.LblName)
        Me.Controls.Add(Me.LblRegistrant)
        Me.Name = "ConferenceRegistrationSystem"
        Me.Text = "Conference Registration System"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LblRegistrant As Label
    Friend WithEvents LblName As Label
    Friend WithEvents LblCompany As Label
    Friend WithEvents LblAddress As Label
    Friend WithEvents LblCity As Label
    Friend WithEvents LblPhone As Label
    Friend WithEvents LblEmail As Label
    Friend WithEvents LblState As Label
    Friend WithEvents LblZip As Label
    Friend WithEvents LblTotal As Label
    Friend WithEvents TextTotal As TextBox
    Friend WithEvents BtnOptions As Button
    Friend WithEvents BtnReset As Button
    Friend WithEvents BtnExit As Button
    Friend WithEvents TextName As TextBox
    Friend WithEvents TextCompany As TextBox
    Friend WithEvents TextAddress As TextBox
    Friend WithEvents TextCity As TextBox
    Friend WithEvents TextPhone As TextBox
    Friend WithEvents TextEmail As TextBox
    Friend WithEvents TextState As TextBox
    Friend WithEvents TextZip As TextBox
End Class
