﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Individual
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblSelectPhone = New System.Windows.Forms.Label()
        Me.RadioModel100 = New System.Windows.Forms.RadioButton()
        Me.RadioModel110 = New System.Windows.Forms.RadioButton()
        Me.RadioModel200 = New System.Windows.Forms.RadioButton()
        Me.Radio800Mins = New System.Windows.Forms.RadioButton()
        Me.Radio1500Mins = New System.Windows.Forms.RadioButton()
        Me.RadioUnlimited = New System.Windows.Forms.RadioButton()
        Me.LblPackage = New System.Windows.Forms.Label()
        Me.LblOptions = New System.Windows.Forms.Label()
        Me.CheckEmail = New System.Windows.Forms.CheckBox()
        Me.CheckText = New System.Windows.Forms.CheckBox()
        Me.BtnCalculate = New System.Windows.Forms.Button()
        Me.BtnClose = New System.Windows.Forms.Button()
        Me.LblTotals = New System.Windows.Forms.Label()
        Me.LblSubtotal = New System.Windows.Forms.Label()
        Me.LblTax = New System.Windows.Forms.Label()
        Me.LblPhoneTotal = New System.Windows.Forms.Label()
        Me.LblExtras = New System.Windows.Forms.Label()
        Me.LblPackageCharge = New System.Windows.Forms.Label()
        Me.LblTotal = New System.Windows.Forms.Label()
        Me.TextSubtotal = New System.Windows.Forms.TextBox()
        Me.TextTax = New System.Windows.Forms.TextBox()
        Me.TextPhoneTotal = New System.Windows.Forms.TextBox()
        Me.TextOptions = New System.Windows.Forms.TextBox()
        Me.TextPackageCharge = New System.Windows.Forms.TextBox()
        Me.TextTotal = New System.Windows.Forms.TextBox()
        Me.PanelPhones = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PanelPhones.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'LblSelectPhone
        '
        Me.LblSelectPhone.AutoSize = True
        Me.LblSelectPhone.Location = New System.Drawing.Point(30, 33)
        Me.LblSelectPhone.Name = "LblSelectPhone"
        Me.LblSelectPhone.Size = New System.Drawing.Size(117, 20)
        Me.LblSelectPhone.TabIndex = 0
        Me.LblSelectPhone.Text = "Select a Phone"
        '
        'RadioModel100
        '
        Me.RadioModel100.AutoSize = True
        Me.RadioModel100.Location = New System.Drawing.Point(27, 3)
        Me.RadioModel100.Name = "RadioModel100"
        Me.RadioModel100.Size = New System.Drawing.Size(108, 24)
        Me.RadioModel100.TabIndex = 1
        Me.RadioModel100.TabStop = True
        Me.RadioModel100.Text = "Model 100"
        Me.RadioModel100.UseVisualStyleBackColor = True
        '
        'RadioModel110
        '
        Me.RadioModel110.AutoSize = True
        Me.RadioModel110.Location = New System.Drawing.Point(27, 42)
        Me.RadioModel110.Name = "RadioModel110"
        Me.RadioModel110.Size = New System.Drawing.Size(108, 24)
        Me.RadioModel110.TabIndex = 2
        Me.RadioModel110.TabStop = True
        Me.RadioModel110.Text = "Model 110"
        Me.RadioModel110.UseVisualStyleBackColor = True
        '
        'RadioModel200
        '
        Me.RadioModel200.AutoSize = True
        Me.RadioModel200.Location = New System.Drawing.Point(27, 82)
        Me.RadioModel200.Name = "RadioModel200"
        Me.RadioModel200.Size = New System.Drawing.Size(108, 24)
        Me.RadioModel200.TabIndex = 3
        Me.RadioModel200.TabStop = True
        Me.RadioModel200.Text = "Model 200"
        Me.RadioModel200.UseVisualStyleBackColor = True
        '
        'Radio800Mins
        '
        Me.Radio800Mins.AutoSize = True
        Me.Radio800Mins.Location = New System.Drawing.Point(22, 3)
        Me.Radio800Mins.Name = "Radio800Mins"
        Me.Radio800Mins.Size = New System.Drawing.Size(197, 24)
        Me.Radio800Mins.TabIndex = 4
        Me.Radio800Mins.TabStop = True
        Me.Radio800Mins.Text = "800 Minutes per Month"
        Me.Radio800Mins.UseVisualStyleBackColor = True
        '
        'Radio1500Mins
        '
        Me.Radio1500Mins.AutoSize = True
        Me.Radio1500Mins.Location = New System.Drawing.Point(22, 42)
        Me.Radio1500Mins.Name = "Radio1500Mins"
        Me.Radio1500Mins.Size = New System.Drawing.Size(206, 24)
        Me.Radio1500Mins.TabIndex = 5
        Me.Radio1500Mins.TabStop = True
        Me.Radio1500Mins.Text = "1500 Minutes per Month"
        Me.Radio1500Mins.UseVisualStyleBackColor = True
        '
        'RadioUnlimited
        '
        Me.RadioUnlimited.AutoSize = True
        Me.RadioUnlimited.Location = New System.Drawing.Point(22, 82)
        Me.RadioUnlimited.Name = "RadioUnlimited"
        Me.RadioUnlimited.Size = New System.Drawing.Size(236, 24)
        Me.RadioUnlimited.TabIndex = 6
        Me.RadioUnlimited.TabStop = True
        Me.RadioUnlimited.Text = "Unlimited Minutes per Month"
        Me.RadioUnlimited.UseVisualStyleBackColor = True
        '
        'LblPackage
        '
        Me.LblPackage.AutoSize = True
        Me.LblPackage.Location = New System.Drawing.Point(234, 33)
        Me.LblPackage.Name = "LblPackage"
        Me.LblPackage.Size = New System.Drawing.Size(133, 20)
        Me.LblPackage.TabIndex = 7
        Me.LblPackage.Text = "Select a Package"
        '
        'LblOptions
        '
        Me.LblOptions.AutoSize = True
        Me.LblOptions.Location = New System.Drawing.Point(30, 208)
        Me.LblOptions.Name = "LblOptions"
        Me.LblOptions.Size = New System.Drawing.Size(113, 20)
        Me.LblOptions.TabIndex = 8
        Me.LblOptions.Text = "Select Options"
        '
        'CheckEmail
        '
        Me.CheckEmail.AutoSize = True
        Me.CheckEmail.Location = New System.Drawing.Point(21, 12)
        Me.CheckEmail.Name = "CheckEmail"
        Me.CheckEmail.Size = New System.Drawing.Size(74, 24)
        Me.CheckEmail.TabIndex = 9
        Me.CheckEmail.Text = "Email"
        Me.CheckEmail.UseVisualStyleBackColor = True
        '
        'CheckText
        '
        Me.CheckText.AutoSize = True
        Me.CheckText.Location = New System.Drawing.Point(21, 46)
        Me.CheckText.Name = "CheckText"
        Me.CheckText.Size = New System.Drawing.Size(146, 24)
        Me.CheckText.TabIndex = 10
        Me.CheckText.Text = "Text Messaging"
        Me.CheckText.UseVisualStyleBackColor = True
        '
        'BtnCalculate
        '
        Me.BtnCalculate.Location = New System.Drawing.Point(13, 351)
        Me.BtnCalculate.Name = "BtnCalculate"
        Me.BtnCalculate.Size = New System.Drawing.Size(202, 56)
        Me.BtnCalculate.TabIndex = 11
        Me.BtnCalculate.Text = "Calculate"
        Me.BtnCalculate.UseVisualStyleBackColor = True
        '
        'BtnClose
        '
        Me.BtnClose.Location = New System.Drawing.Point(13, 413)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Size = New System.Drawing.Size(202, 56)
        Me.BtnClose.TabIndex = 12
        Me.BtnClose.Text = "Close"
        Me.BtnClose.UseVisualStyleBackColor = True
        '
        'LblTotals
        '
        Me.LblTotals.AutoSize = True
        Me.LblTotals.Location = New System.Drawing.Point(238, 231)
        Me.LblTotals.Name = "LblTotals"
        Me.LblTotals.Size = New System.Drawing.Size(52, 20)
        Me.LblTotals.TabIndex = 13
        Me.LblTotals.Text = "Totals"
        '
        'LblSubtotal
        '
        Me.LblSubtotal.AutoSize = True
        Me.LblSubtotal.Location = New System.Drawing.Point(275, 271)
        Me.LblSubtotal.Name = "LblSubtotal"
        Me.LblSubtotal.Size = New System.Drawing.Size(119, 20)
        Me.LblSubtotal.TabIndex = 14
        Me.LblSubtotal.Text = "Phone Subtotal"
        '
        'LblTax
        '
        Me.LblTax.AutoSize = True
        Me.LblTax.Location = New System.Drawing.Point(360, 307)
        Me.LblTax.Name = "LblTax"
        Me.LblTax.Size = New System.Drawing.Size(34, 20)
        Me.LblTax.TabIndex = 15
        Me.LblTax.Text = "Tax"
        '
        'LblPhoneTotal
        '
        Me.LblPhoneTotal.AutoSize = True
        Me.LblPhoneTotal.Location = New System.Drawing.Point(300, 342)
        Me.LblPhoneTotal.Name = "LblPhoneTotal"
        Me.LblPhoneTotal.Size = New System.Drawing.Size(94, 20)
        Me.LblPhoneTotal.TabIndex = 16
        Me.LblPhoneTotal.Text = "Phone Total"
        '
        'LblExtras
        '
        Me.LblExtras.AutoSize = True
        Me.LblExtras.Location = New System.Drawing.Point(330, 378)
        Me.LblExtras.Name = "LblExtras"
        Me.LblExtras.Size = New System.Drawing.Size(64, 20)
        Me.LblExtras.TabIndex = 17
        Me.LblExtras.Text = "Options"
        '
        'LblPackageCharge
        '
        Me.LblPackageCharge.AutoSize = True
        Me.LblPackageCharge.Location = New System.Drawing.Point(267, 413)
        Me.LblPackageCharge.Name = "LblPackageCharge"
        Me.LblPackageCharge.Size = New System.Drawing.Size(127, 20)
        Me.LblPackageCharge.TabIndex = 18
        Me.LblPackageCharge.Text = "Package Charge"
        '
        'LblTotal
        '
        Me.LblTotal.AutoSize = True
        Me.LblTotal.Location = New System.Drawing.Point(235, 449)
        Me.LblTotal.Name = "LblTotal"
        Me.LblTotal.Size = New System.Drawing.Size(159, 20)
        Me.LblTotal.TabIndex = 19
        Me.LblTotal.Text = "Total Monthly Charge"
        '
        'TextSubtotal
        '
        Me.TextSubtotal.Enabled = False
        Me.TextSubtotal.Location = New System.Drawing.Point(431, 271)
        Me.TextSubtotal.Name = "TextSubtotal"
        Me.TextSubtotal.Size = New System.Drawing.Size(100, 26)
        Me.TextSubtotal.TabIndex = 20
        '
        'TextTax
        '
        Me.TextTax.Enabled = False
        Me.TextTax.Location = New System.Drawing.Point(431, 305)
        Me.TextTax.Name = "TextTax"
        Me.TextTax.Size = New System.Drawing.Size(100, 26)
        Me.TextTax.TabIndex = 21
        '
        'TextPhoneTotal
        '
        Me.TextPhoneTotal.Enabled = False
        Me.TextPhoneTotal.Location = New System.Drawing.Point(431, 342)
        Me.TextPhoneTotal.Name = "TextPhoneTotal"
        Me.TextPhoneTotal.Size = New System.Drawing.Size(100, 26)
        Me.TextPhoneTotal.TabIndex = 22
        '
        'TextOptions
        '
        Me.TextOptions.Enabled = False
        Me.TextOptions.Location = New System.Drawing.Point(431, 378)
        Me.TextOptions.Name = "TextOptions"
        Me.TextOptions.Size = New System.Drawing.Size(100, 26)
        Me.TextOptions.TabIndex = 23
        '
        'TextPackageCharge
        '
        Me.TextPackageCharge.Enabled = False
        Me.TextPackageCharge.Location = New System.Drawing.Point(431, 413)
        Me.TextPackageCharge.Name = "TextPackageCharge"
        Me.TextPackageCharge.Size = New System.Drawing.Size(100, 26)
        Me.TextPackageCharge.TabIndex = 24
        '
        'TextTotal
        '
        Me.TextTotal.Enabled = False
        Me.TextTotal.Location = New System.Drawing.Point(431, 449)
        Me.TextTotal.Name = "TextTotal"
        Me.TextTotal.Size = New System.Drawing.Size(100, 26)
        Me.TextTotal.TabIndex = 25
        '
        'PanelPhones
        '
        Me.PanelPhones.Controls.Add(Me.RadioModel200)
        Me.PanelPhones.Controls.Add(Me.RadioModel110)
        Me.PanelPhones.Controls.Add(Me.RadioModel100)
        Me.PanelPhones.Location = New System.Drawing.Point(13, 69)
        Me.PanelPhones.Name = "PanelPhones"
        Me.PanelPhones.Size = New System.Drawing.Size(175, 118)
        Me.PanelPhones.TabIndex = 26
        Me.PanelPhones.Tag = ""
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.CheckEmail)
        Me.Panel1.Controls.Add(Me.CheckText)
        Me.Panel1.Location = New System.Drawing.Point(13, 240)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(176, 73)
        Me.Panel1.TabIndex = 27
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Radio800Mins)
        Me.Panel2.Controls.Add(Me.Radio1500Mins)
        Me.Panel2.Controls.Add(Me.RadioUnlimited)
        Me.Panel2.Location = New System.Drawing.Point(228, 69)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(266, 118)
        Me.Panel2.TabIndex = 28
        '
        'Individual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(555, 487)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.PanelPhones)
        Me.Controls.Add(Me.TextTotal)
        Me.Controls.Add(Me.TextPackageCharge)
        Me.Controls.Add(Me.LblSelectPhone)
        Me.Controls.Add(Me.TextOptions)
        Me.Controls.Add(Me.TextPhoneTotal)
        Me.Controls.Add(Me.TextTax)
        Me.Controls.Add(Me.TextSubtotal)
        Me.Controls.Add(Me.LblTotal)
        Me.Controls.Add(Me.LblPackageCharge)
        Me.Controls.Add(Me.LblExtras)
        Me.Controls.Add(Me.LblPhoneTotal)
        Me.Controls.Add(Me.LblTax)
        Me.Controls.Add(Me.LblSubtotal)
        Me.Controls.Add(Me.LblTotals)
        Me.Controls.Add(Me.BtnClose)
        Me.Controls.Add(Me.BtnCalculate)
        Me.Controls.Add(Me.LblOptions)
        Me.Controls.Add(Me.LblPackage)
        Me.Name = "Individual"
        Me.Text = "Individual Plan"
        Me.PanelPhones.ResumeLayout(False)
        Me.PanelPhones.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LblSelectPhone As Label
    Friend WithEvents RadioModel100 As RadioButton
    Friend WithEvents RadioModel110 As RadioButton
    Friend WithEvents RadioModel200 As RadioButton
    Friend WithEvents Radio800Mins As RadioButton
    Friend WithEvents Radio1500Mins As RadioButton
    Friend WithEvents RadioUnlimited As RadioButton
    Friend WithEvents LblPackage As Label
    Friend WithEvents LblOptions As Label
    Friend WithEvents CheckEmail As CheckBox
    Friend WithEvents CheckText As CheckBox
    Friend WithEvents BtnCalculate As Button
    Friend WithEvents BtnClose As Button
    Friend WithEvents LblTotals As Label
    Friend WithEvents LblSubtotal As Label
    Friend WithEvents LblTax As Label
    Friend WithEvents LblPhoneTotal As Label
    Friend WithEvents LblExtras As Label
    Friend WithEvents LblPackageCharge As Label
    Friend WithEvents LblTotal As Label
    Friend WithEvents TextSubtotal As TextBox
    Friend WithEvents TextTax As TextBox
    Friend WithEvents TextPhoneTotal As TextBox
    Friend WithEvents TextOptions As TextBox
    Friend WithEvents TextPackageCharge As TextBox
    Friend WithEvents TextTotal As TextBox
    Friend WithEvents PanelPhones As Panel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
End Class
