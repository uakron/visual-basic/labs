﻿Public Class CellPhonePackages
    Private Sub BtnExit_Click(sender As Object, e As EventArgs) Handles BtnExit.Click
        Me.Close()
    End Sub

    Private Sub BtnIndividual_Click(sender As Object, e As EventArgs) Handles BtnIndividual.Click
        Individual.ShowDialog()
    End Sub

    Private Sub BtnFamily_Click(sender As Object, e As EventArgs) Handles BtnFamily.Click
        Family.ShowDialog()
    End Sub
End Class
