﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Family
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Radio800Mins = New System.Windows.Forms.RadioButton()
        Me.Radio1500Mins = New System.Windows.Forms.RadioButton()
        Me.RadioUnlimited = New System.Windows.Forms.RadioButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CheckEmail = New System.Windows.Forms.CheckBox()
        Me.CheckText = New System.Windows.Forms.CheckBox()
        Me.PanelPhones = New System.Windows.Forms.Panel()
        Me.RadioModel200 = New System.Windows.Forms.RadioButton()
        Me.RadioModel110 = New System.Windows.Forms.RadioButton()
        Me.RadioModel100 = New System.Windows.Forms.RadioButton()
        Me.TextTotal = New System.Windows.Forms.TextBox()
        Me.TextPackageCharge = New System.Windows.Forms.TextBox()
        Me.LblSelectPhone = New System.Windows.Forms.Label()
        Me.TextOptions = New System.Windows.Forms.TextBox()
        Me.TextPhoneTotal = New System.Windows.Forms.TextBox()
        Me.TextTax = New System.Windows.Forms.TextBox()
        Me.TextSubtotal = New System.Windows.Forms.TextBox()
        Me.LblTotal = New System.Windows.Forms.Label()
        Me.LblPackageCharge = New System.Windows.Forms.Label()
        Me.LblExtras = New System.Windows.Forms.Label()
        Me.LblPhoneTotal = New System.Windows.Forms.Label()
        Me.LblTax = New System.Windows.Forms.Label()
        Me.LblSubtotal = New System.Windows.Forms.Label()
        Me.LblTotals = New System.Windows.Forms.Label()
        Me.BtnClose = New System.Windows.Forms.Button()
        Me.BtnCalculate = New System.Windows.Forms.Button()
        Me.LblOptions = New System.Windows.Forms.Label()
        Me.LblPackage = New System.Windows.Forms.Label()
        Me.LblNumPhones = New System.Windows.Forms.Label()
        Me.TextNumPhones = New System.Windows.Forms.TextBox()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.PanelPhones.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Radio800Mins)
        Me.Panel2.Controls.Add(Me.Radio1500Mins)
        Me.Panel2.Controls.Add(Me.RadioUnlimited)
        Me.Panel2.Location = New System.Drawing.Point(231, 97)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(266, 118)
        Me.Panel2.TabIndex = 49
        '
        'Radio800Mins
        '
        Me.Radio800Mins.AutoSize = True
        Me.Radio800Mins.Location = New System.Drawing.Point(22, 3)
        Me.Radio800Mins.Name = "Radio800Mins"
        Me.Radio800Mins.Size = New System.Drawing.Size(197, 24)
        Me.Radio800Mins.TabIndex = 4
        Me.Radio800Mins.TabStop = True
        Me.Radio800Mins.Text = "800 Minutes per Month"
        Me.Radio800Mins.UseVisualStyleBackColor = True
        '
        'Radio1500Mins
        '
        Me.Radio1500Mins.AutoSize = True
        Me.Radio1500Mins.Location = New System.Drawing.Point(22, 42)
        Me.Radio1500Mins.Name = "Radio1500Mins"
        Me.Radio1500Mins.Size = New System.Drawing.Size(206, 24)
        Me.Radio1500Mins.TabIndex = 5
        Me.Radio1500Mins.TabStop = True
        Me.Radio1500Mins.Text = "1500 Minutes per Month"
        Me.Radio1500Mins.UseVisualStyleBackColor = True
        '
        'RadioUnlimited
        '
        Me.RadioUnlimited.AutoSize = True
        Me.RadioUnlimited.Location = New System.Drawing.Point(22, 82)
        Me.RadioUnlimited.Name = "RadioUnlimited"
        Me.RadioUnlimited.Size = New System.Drawing.Size(236, 24)
        Me.RadioUnlimited.TabIndex = 6
        Me.RadioUnlimited.TabStop = True
        Me.RadioUnlimited.Text = "Unlimited Minutes per Month"
        Me.RadioUnlimited.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.CheckEmail)
        Me.Panel1.Controls.Add(Me.CheckText)
        Me.Panel1.Location = New System.Drawing.Point(16, 268)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(176, 73)
        Me.Panel1.TabIndex = 48
        '
        'CheckEmail
        '
        Me.CheckEmail.AutoSize = True
        Me.CheckEmail.Location = New System.Drawing.Point(21, 12)
        Me.CheckEmail.Name = "CheckEmail"
        Me.CheckEmail.Size = New System.Drawing.Size(74, 24)
        Me.CheckEmail.TabIndex = 9
        Me.CheckEmail.Text = "Email"
        Me.CheckEmail.UseVisualStyleBackColor = True
        '
        'CheckText
        '
        Me.CheckText.AutoSize = True
        Me.CheckText.Location = New System.Drawing.Point(21, 46)
        Me.CheckText.Name = "CheckText"
        Me.CheckText.Size = New System.Drawing.Size(146, 24)
        Me.CheckText.TabIndex = 10
        Me.CheckText.Text = "Text Messaging"
        Me.CheckText.UseVisualStyleBackColor = True
        '
        'PanelPhones
        '
        Me.PanelPhones.Controls.Add(Me.RadioModel200)
        Me.PanelPhones.Controls.Add(Me.RadioModel110)
        Me.PanelPhones.Controls.Add(Me.RadioModel100)
        Me.PanelPhones.Location = New System.Drawing.Point(16, 97)
        Me.PanelPhones.Name = "PanelPhones"
        Me.PanelPhones.Size = New System.Drawing.Size(175, 118)
        Me.PanelPhones.TabIndex = 47
        Me.PanelPhones.Tag = ""
        '
        'RadioModel200
        '
        Me.RadioModel200.AutoSize = True
        Me.RadioModel200.Location = New System.Drawing.Point(27, 82)
        Me.RadioModel200.Name = "RadioModel200"
        Me.RadioModel200.Size = New System.Drawing.Size(108, 24)
        Me.RadioModel200.TabIndex = 3
        Me.RadioModel200.TabStop = True
        Me.RadioModel200.Text = "Model 200"
        Me.RadioModel200.UseVisualStyleBackColor = True
        '
        'RadioModel110
        '
        Me.RadioModel110.AutoSize = True
        Me.RadioModel110.Location = New System.Drawing.Point(27, 42)
        Me.RadioModel110.Name = "RadioModel110"
        Me.RadioModel110.Size = New System.Drawing.Size(108, 24)
        Me.RadioModel110.TabIndex = 2
        Me.RadioModel110.TabStop = True
        Me.RadioModel110.Text = "Model 110"
        Me.RadioModel110.UseVisualStyleBackColor = True
        '
        'RadioModel100
        '
        Me.RadioModel100.AutoSize = True
        Me.RadioModel100.Location = New System.Drawing.Point(27, 3)
        Me.RadioModel100.Name = "RadioModel100"
        Me.RadioModel100.Size = New System.Drawing.Size(108, 24)
        Me.RadioModel100.TabIndex = 1
        Me.RadioModel100.TabStop = True
        Me.RadioModel100.Text = "Model 100"
        Me.RadioModel100.UseVisualStyleBackColor = True
        '
        'TextTotal
        '
        Me.TextTotal.Enabled = False
        Me.TextTotal.Location = New System.Drawing.Point(434, 477)
        Me.TextTotal.Name = "TextTotal"
        Me.TextTotal.Size = New System.Drawing.Size(100, 26)
        Me.TextTotal.TabIndex = 46
        '
        'TextPackageCharge
        '
        Me.TextPackageCharge.Enabled = False
        Me.TextPackageCharge.Location = New System.Drawing.Point(434, 441)
        Me.TextPackageCharge.Name = "TextPackageCharge"
        Me.TextPackageCharge.Size = New System.Drawing.Size(100, 26)
        Me.TextPackageCharge.TabIndex = 45
        '
        'LblSelectPhone
        '
        Me.LblSelectPhone.AutoSize = True
        Me.LblSelectPhone.Location = New System.Drawing.Point(33, 61)
        Me.LblSelectPhone.Name = "LblSelectPhone"
        Me.LblSelectPhone.Size = New System.Drawing.Size(117, 20)
        Me.LblSelectPhone.TabIndex = 29
        Me.LblSelectPhone.Text = "Select a Phone"
        '
        'TextOptions
        '
        Me.TextOptions.Enabled = False
        Me.TextOptions.Location = New System.Drawing.Point(434, 406)
        Me.TextOptions.Name = "TextOptions"
        Me.TextOptions.Size = New System.Drawing.Size(100, 26)
        Me.TextOptions.TabIndex = 44
        '
        'TextPhoneTotal
        '
        Me.TextPhoneTotal.Enabled = False
        Me.TextPhoneTotal.Location = New System.Drawing.Point(434, 370)
        Me.TextPhoneTotal.Name = "TextPhoneTotal"
        Me.TextPhoneTotal.Size = New System.Drawing.Size(100, 26)
        Me.TextPhoneTotal.TabIndex = 43
        '
        'TextTax
        '
        Me.TextTax.Enabled = False
        Me.TextTax.Location = New System.Drawing.Point(434, 333)
        Me.TextTax.Name = "TextTax"
        Me.TextTax.Size = New System.Drawing.Size(100, 26)
        Me.TextTax.TabIndex = 42
        '
        'TextSubtotal
        '
        Me.TextSubtotal.Enabled = False
        Me.TextSubtotal.Location = New System.Drawing.Point(434, 299)
        Me.TextSubtotal.Name = "TextSubtotal"
        Me.TextSubtotal.Size = New System.Drawing.Size(100, 26)
        Me.TextSubtotal.TabIndex = 41
        '
        'LblTotal
        '
        Me.LblTotal.AutoSize = True
        Me.LblTotal.Location = New System.Drawing.Point(238, 477)
        Me.LblTotal.Name = "LblTotal"
        Me.LblTotal.Size = New System.Drawing.Size(159, 20)
        Me.LblTotal.TabIndex = 40
        Me.LblTotal.Text = "Total Monthly Charge"
        '
        'LblPackageCharge
        '
        Me.LblPackageCharge.AutoSize = True
        Me.LblPackageCharge.Location = New System.Drawing.Point(270, 441)
        Me.LblPackageCharge.Name = "LblPackageCharge"
        Me.LblPackageCharge.Size = New System.Drawing.Size(127, 20)
        Me.LblPackageCharge.TabIndex = 39
        Me.LblPackageCharge.Text = "Package Charge"
        '
        'LblExtras
        '
        Me.LblExtras.AutoSize = True
        Me.LblExtras.Location = New System.Drawing.Point(333, 406)
        Me.LblExtras.Name = "LblExtras"
        Me.LblExtras.Size = New System.Drawing.Size(64, 20)
        Me.LblExtras.TabIndex = 38
        Me.LblExtras.Text = "Options"
        '
        'LblPhoneTotal
        '
        Me.LblPhoneTotal.AutoSize = True
        Me.LblPhoneTotal.Location = New System.Drawing.Point(303, 370)
        Me.LblPhoneTotal.Name = "LblPhoneTotal"
        Me.LblPhoneTotal.Size = New System.Drawing.Size(94, 20)
        Me.LblPhoneTotal.TabIndex = 37
        Me.LblPhoneTotal.Text = "Phone Total"
        '
        'LblTax
        '
        Me.LblTax.AutoSize = True
        Me.LblTax.Location = New System.Drawing.Point(363, 335)
        Me.LblTax.Name = "LblTax"
        Me.LblTax.Size = New System.Drawing.Size(34, 20)
        Me.LblTax.TabIndex = 36
        Me.LblTax.Text = "Tax"
        '
        'LblSubtotal
        '
        Me.LblSubtotal.AutoSize = True
        Me.LblSubtotal.Location = New System.Drawing.Point(278, 299)
        Me.LblSubtotal.Name = "LblSubtotal"
        Me.LblSubtotal.Size = New System.Drawing.Size(119, 20)
        Me.LblSubtotal.TabIndex = 35
        Me.LblSubtotal.Text = "Phone Subtotal"
        '
        'LblTotals
        '
        Me.LblTotals.AutoSize = True
        Me.LblTotals.Location = New System.Drawing.Point(241, 259)
        Me.LblTotals.Name = "LblTotals"
        Me.LblTotals.Size = New System.Drawing.Size(52, 20)
        Me.LblTotals.TabIndex = 34
        Me.LblTotals.Text = "Totals"
        '
        'BtnClose
        '
        Me.BtnClose.Location = New System.Drawing.Point(16, 441)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Size = New System.Drawing.Size(202, 56)
        Me.BtnClose.TabIndex = 33
        Me.BtnClose.Text = "Close"
        Me.BtnClose.UseVisualStyleBackColor = True
        '
        'BtnCalculate
        '
        Me.BtnCalculate.Location = New System.Drawing.Point(16, 379)
        Me.BtnCalculate.Name = "BtnCalculate"
        Me.BtnCalculate.Size = New System.Drawing.Size(202, 56)
        Me.BtnCalculate.TabIndex = 32
        Me.BtnCalculate.Text = "Calculate"
        Me.BtnCalculate.UseVisualStyleBackColor = True
        '
        'LblOptions
        '
        Me.LblOptions.AutoSize = True
        Me.LblOptions.Location = New System.Drawing.Point(33, 236)
        Me.LblOptions.Name = "LblOptions"
        Me.LblOptions.Size = New System.Drawing.Size(113, 20)
        Me.LblOptions.TabIndex = 31
        Me.LblOptions.Text = "Select Options"
        '
        'LblPackage
        '
        Me.LblPackage.AutoSize = True
        Me.LblPackage.Location = New System.Drawing.Point(237, 61)
        Me.LblPackage.Name = "LblPackage"
        Me.LblPackage.Size = New System.Drawing.Size(133, 20)
        Me.LblPackage.TabIndex = 30
        Me.LblPackage.Text = "Select a Package"
        '
        'LblNumPhones
        '
        Me.LblNumPhones.AutoSize = True
        Me.LblNumPhones.Location = New System.Drawing.Point(33, 19)
        Me.LblNumPhones.Name = "LblNumPhones"
        Me.LblNumPhones.Size = New System.Drawing.Size(141, 20)
        Me.LblNumPhones.TabIndex = 50
        Me.LblNumPhones.Text = "Number of Phones"
        '
        'TextNumPhones
        '
        Me.TextNumPhones.Location = New System.Drawing.Point(231, 19)
        Me.TextNumPhones.Name = "TextNumPhones"
        Me.TextNumPhones.Size = New System.Drawing.Size(100, 26)
        Me.TextNumPhones.TabIndex = 51
        '
        'Family
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(551, 517)
        Me.Controls.Add(Me.TextNumPhones)
        Me.Controls.Add(Me.LblNumPhones)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.PanelPhones)
        Me.Controls.Add(Me.TextTotal)
        Me.Controls.Add(Me.TextPackageCharge)
        Me.Controls.Add(Me.LblSelectPhone)
        Me.Controls.Add(Me.TextOptions)
        Me.Controls.Add(Me.TextPhoneTotal)
        Me.Controls.Add(Me.TextTax)
        Me.Controls.Add(Me.TextSubtotal)
        Me.Controls.Add(Me.LblTotal)
        Me.Controls.Add(Me.LblPackageCharge)
        Me.Controls.Add(Me.LblExtras)
        Me.Controls.Add(Me.LblPhoneTotal)
        Me.Controls.Add(Me.LblTax)
        Me.Controls.Add(Me.LblSubtotal)
        Me.Controls.Add(Me.LblTotals)
        Me.Controls.Add(Me.BtnClose)
        Me.Controls.Add(Me.BtnCalculate)
        Me.Controls.Add(Me.LblOptions)
        Me.Controls.Add(Me.LblPackage)
        Me.Name = "Family"
        Me.Text = "Family Plan"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.PanelPhones.ResumeLayout(False)
        Me.PanelPhones.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel2 As Panel
    Friend WithEvents Radio800Mins As RadioButton
    Friend WithEvents Radio1500Mins As RadioButton
    Friend WithEvents RadioUnlimited As RadioButton
    Friend WithEvents Panel1 As Panel
    Friend WithEvents CheckEmail As CheckBox
    Friend WithEvents CheckText As CheckBox
    Friend WithEvents PanelPhones As Panel
    Friend WithEvents RadioModel200 As RadioButton
    Friend WithEvents RadioModel110 As RadioButton
    Friend WithEvents RadioModel100 As RadioButton
    Friend WithEvents TextTotal As TextBox
    Friend WithEvents TextPackageCharge As TextBox
    Friend WithEvents LblSelectPhone As Label
    Friend WithEvents TextOptions As TextBox
    Friend WithEvents TextPhoneTotal As TextBox
    Friend WithEvents TextTax As TextBox
    Friend WithEvents TextSubtotal As TextBox
    Friend WithEvents LblTotal As Label
    Friend WithEvents LblPackageCharge As Label
    Friend WithEvents LblExtras As Label
    Friend WithEvents LblPhoneTotal As Label
    Friend WithEvents LblTax As Label
    Friend WithEvents LblSubtotal As Label
    Friend WithEvents LblTotals As Label
    Friend WithEvents BtnClose As Button
    Friend WithEvents BtnCalculate As Button
    Friend WithEvents LblOptions As Label
    Friend WithEvents LblPackage As Label
    Friend WithEvents LblNumPhones As Label
    Friend WithEvents TextNumPhones As TextBox
End Class
