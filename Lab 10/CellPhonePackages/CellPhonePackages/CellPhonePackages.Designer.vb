﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CellPhonePackages
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblIndividual = New System.Windows.Forms.Label()
        Me.LblFamily = New System.Windows.Forms.Label()
        Me.BtnIndividual = New System.Windows.Forms.Button()
        Me.BtnFamily = New System.Windows.Forms.Button()
        Me.BtnExit = New System.Windows.Forms.Button()
        Me.LblSelect = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'LblIndividual
        '
        Me.LblIndividual.Location = New System.Drawing.Point(45, 71)
        Me.LblIndividual.Name = "LblIndividual"
        Me.LblIndividual.Size = New System.Drawing.Size(165, 110)
        Me.LblIndividual.TabIndex = 0
        Me.LblIndividual.Text = "The Individual plan provides one cell phone and a variety of monthly packages."
        '
        'LblFamily
        '
        Me.LblFamily.Location = New System.Drawing.Point(45, 212)
        Me.LblFamily.Name = "LblFamily"
        Me.LblFamily.Size = New System.Drawing.Size(165, 110)
        Me.LblFamily.TabIndex = 1
        Me.LblFamily.Text = "The Family plan allows you to purchase multiple cell phones of the same model, wi" &
    "th each phone sharing one monthly package."
        '
        'BtnIndividual
        '
        Me.BtnIndividual.Location = New System.Drawing.Point(310, 71)
        Me.BtnIndividual.Name = "BtnIndividual"
        Me.BtnIndividual.Size = New System.Drawing.Size(150, 75)
        Me.BtnIndividual.TabIndex = 2
        Me.BtnIndividual.Text = "Individual"
        Me.BtnIndividual.UseVisualStyleBackColor = True
        '
        'BtnFamily
        '
        Me.BtnFamily.Location = New System.Drawing.Point(310, 212)
        Me.BtnFamily.Name = "BtnFamily"
        Me.BtnFamily.Size = New System.Drawing.Size(150, 75)
        Me.BtnFamily.TabIndex = 3
        Me.BtnFamily.Text = "Family"
        Me.BtnFamily.UseVisualStyleBackColor = True
        '
        'BtnExit
        '
        Me.BtnExit.Location = New System.Drawing.Point(419, 335)
        Me.BtnExit.Name = "BtnExit"
        Me.BtnExit.Size = New System.Drawing.Size(100, 45)
        Me.BtnExit.TabIndex = 4
        Me.BtnExit.Text = "Exit"
        Me.BtnExit.UseVisualStyleBackColor = True
        '
        'LblSelect
        '
        Me.LblSelect.AutoSize = True
        Me.LblSelect.Location = New System.Drawing.Point(12, 18)
        Me.LblSelect.Name = "LblSelect"
        Me.LblSelect.Size = New System.Drawing.Size(102, 20)
        Me.LblSelect.TabIndex = 5
        Me.LblSelect.Text = "Select a Plan"
        '
        'CellPhonePackages
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(531, 392)
        Me.Controls.Add(Me.LblSelect)
        Me.Controls.Add(Me.BtnExit)
        Me.Controls.Add(Me.BtnFamily)
        Me.Controls.Add(Me.BtnIndividual)
        Me.Controls.Add(Me.LblFamily)
        Me.Controls.Add(Me.LblIndividual)
        Me.Name = "CellPhonePackages"
        Me.Text = "Cell Phone Packages"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LblIndividual As Label
    Friend WithEvents LblFamily As Label
    Friend WithEvents BtnIndividual As Button
    Friend WithEvents BtnFamily As Button
    Friend WithEvents BtnExit As Button
    Friend WithEvents LblSelect As Label
End Class
