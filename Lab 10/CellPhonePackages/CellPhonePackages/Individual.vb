﻿Public Class Individual
    Private Sub BtnClose_Click(sender As Object, e As EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub BtnCalculate_Click(sender As Object, e As EventArgs) Handles BtnCalculate.Click
        Dim decSubtotal As Decimal
        Dim decTax As Decimal
        Dim decPhoneTotal As Decimal
        Dim decOptions As Decimal
        Dim decPackage As Decimal
        Dim decTotal As Decimal

        If RadioModel100.Checked = True Then
            decSubtotal = 29.95
            decTotal += 29.95
            decTax = 0.06 * decSubtotal
            decPhoneTotal = decSubtotal + decTax
            TextSubtotal.Text = decSubtotal.ToString("c")
            TextTax.Text = decTax.ToString("c")
            TextPhoneTotal.Text = decPhoneTotal.ToString("c")
        End If

        If RadioModel110.Checked = True Then
            decSubtotal = 49.95
            decTotal += 49.95
            decTax = 0.06 * decSubtotal
            decPhoneTotal = decSubtotal + decTax
            TextSubtotal.Text = decSubtotal.ToString("c")
            TextTax.Text = decTax.ToString("c")
            TextPhoneTotal.Text = decPhoneTotal.ToString("c")
        End If

        If RadioModel200.Checked = True Then
            decSubtotal = 99.95
            decTotal += 99.95
            decTax = 0.06 * decSubtotal
            decPhoneTotal = decSubtotal + decTax
            TextSubtotal.Text = decSubtotal.ToString("c")
            TextTax.Text = decTax.ToString("c")
            TextPhoneTotal.Text = decPhoneTotal.ToString("c")
        End If

        If Radio800Mins.Checked = True Then
            decPackage = 45.0
            decTotal += 45.0
            TextPackageCharge.Text = decPackage.ToString("c")
        End If

        If Radio1500Mins.Checked = True Then
            decPackage = 65.0
            decTotal += 65.0
            TextPackageCharge.Text = decPackage.ToString("c")
        End If

        If RadioUnlimited.Checked = True Then
            decPackage = 99.0
            decTotal += 99.0
            TextPackageCharge.Text = decPackage.ToString("c")
        End If

        If CheckEmail.Checked = True Then
            decOptions += 25.0
            decTotal += 25.0
            TextOptions.Text = decOptions.ToString("c")
        End If

        If CheckText.Checked = True Then
            decOptions += 10.0
            decTotal += 10.0
            TextOptions.Text = decOptions.ToString("c")
        End If

        decTotal = decPhoneTotal + decOptions + decPackage
        TextTotal.Text = decTotal.ToString("c")
    End Sub
End Class