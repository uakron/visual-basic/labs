﻿Public Class FormSailboatRaces
    Private Sub ButtonClose_Click(sender As Object, e As EventArgs) Handles ButtonClose.Click
        Me.Close()
    End Sub

    Private Sub ButtonClear_Click(sender As Object, e As EventArgs) Handles ButtonClear.Click
        TextBoat1Race1.Clear()
        TextBoat1Race2.Clear()
        TextBoat1Race3.Clear()
        TextBoat1Race4.Clear()
        TextBoat2Race1.Clear()
        TextBoat2Race2.Clear()
        TextBoat2Race3.Clear()
        TextBoat2Race4.Clear()
        TextBoat3Race1.Clear()
        TextBoat3Race2.Clear()
        TextBoat3Race3.Clear()
        TextBoat3Race4.Clear()
        TextBoat1Total.Clear()
        TextBoat2Total.Clear()
        TextBoat3Total.Clear()
    End Sub

    Private Sub ButtonCalculate_Click(sender As Object, e As EventArgs) Handles ButtonCalculate.Click
        Try
            Dim boat1Race1, boat1Race2, boat1Race3, boat1Race4, boat2Race1, boat2Race2, boat2Race3, boat2Race4, boat3Race1, boat3Race2, boat3Race3, boat3Race4 As Integer
            Dim boat1Total, boat2Total, boat3Total As Integer

            boat1Race1 = CInt(TextBoat1Race1.Text)
            boat1Race2 = CInt(TextBoat1Race2.Text)
            boat1Race3 = CInt(TextBoat1Race3.Text)
            boat1Race4 = CInt(TextBoat1Race4.Text)
            boat2Race1 = CInt(TextBoat2Race1.Text)
            boat2Race2 = CInt(TextBoat2Race2.Text)
            boat2Race3 = CInt(TextBoat2Race3.Text)
            boat2Race4 = CInt(TextBoat2Race4.Text)
            boat3Race1 = CInt(TextBoat3Race1.Text)
            boat3Race2 = CInt(TextBoat3Race2.Text)
            boat3Race3 = CInt(TextBoat3Race3.Text)
            boat3Race4 = CInt(TextBoat3Race4.Text)

            boat1Total = boat1Race1 + boat1Race2 + boat1Race3 + boat1Race4
            boat2Total = boat2Race1 + boat2Race2 + boat2Race3 + boat2Race4
            boat3Total = boat3Race1 + boat3Race2 + boat3Race3 + boat3Race4

            TextBoat1Total.Text = boat1Total
            TextBoat2Total.Text = boat2Total
            TextBoat3Total.Text = boat3Total

            LblStatusErrorMssg.Text = ""
        Catch ex As Exception
            LblStatusErrorMssg.Text = "Please enter valid numbers only."
        End Try
    End Sub
End Class
