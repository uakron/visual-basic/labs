﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormSailboatRaces
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.LabelRace1 = New System.Windows.Forms.Label()
        Me.LabelRace2 = New System.Windows.Forms.Label()
        Me.LabelRace3 = New System.Windows.Forms.Label()
        Me.LabelRace4 = New System.Windows.Forms.Label()
        Me.LabelTotal = New System.Windows.Forms.Label()
        Me.LabelBoat1 = New System.Windows.Forms.Label()
        Me.LabelBoat2 = New System.Windows.Forms.Label()
        Me.LabelBoat3 = New System.Windows.Forms.Label()
        Me.TextBoat1Race1 = New System.Windows.Forms.TextBox()
        Me.TextBoat1Race2 = New System.Windows.Forms.TextBox()
        Me.TextBoat1Race3 = New System.Windows.Forms.TextBox()
        Me.TextBoat1Race4 = New System.Windows.Forms.TextBox()
        Me.TextBoat2Race1 = New System.Windows.Forms.TextBox()
        Me.TextBoat2Race2 = New System.Windows.Forms.TextBox()
        Me.TextBoat2Race3 = New System.Windows.Forms.TextBox()
        Me.TextBoat2Race4 = New System.Windows.Forms.TextBox()
        Me.TextBoat3Race1 = New System.Windows.Forms.TextBox()
        Me.TextBoat3Race2 = New System.Windows.Forms.TextBox()
        Me.TextBoat3Race3 = New System.Windows.Forms.TextBox()
        Me.TextBoat3Race4 = New System.Windows.Forms.TextBox()
        Me.ButtonCalculate = New System.Windows.Forms.Button()
        Me.ButtonClear = New System.Windows.Forms.Button()
        Me.ButtonClose = New System.Windows.Forms.Button()
        Me.TextBoat1Total = New System.Windows.Forms.TextBox()
        Me.TextBoat2Total = New System.Windows.Forms.TextBox()
        Me.TextBoat3Total = New System.Windows.Forms.TextBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.LblStatusErrorMssg = New System.Windows.Forms.ToolStripStatusLabel()
        Me.LabelRaceResults = New System.Windows.Forms.Label()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'LabelRace1
        '
        Me.LabelRace1.AutoSize = True
        Me.LabelRace1.Location = New System.Drawing.Point(161, 88)
        Me.LabelRace1.Name = "LabelRace1"
        Me.LabelRace1.Size = New System.Drawing.Size(60, 20)
        Me.LabelRace1.TabIndex = 0
        Me.LabelRace1.Text = "Race 1"
        '
        'LabelRace2
        '
        Me.LabelRace2.AutoSize = True
        Me.LabelRace2.Location = New System.Drawing.Point(262, 88)
        Me.LabelRace2.Name = "LabelRace2"
        Me.LabelRace2.Size = New System.Drawing.Size(60, 20)
        Me.LabelRace2.TabIndex = 1
        Me.LabelRace2.Text = "Race 2"
        '
        'LabelRace3
        '
        Me.LabelRace3.AutoSize = True
        Me.LabelRace3.Location = New System.Drawing.Point(368, 88)
        Me.LabelRace3.Name = "LabelRace3"
        Me.LabelRace3.Size = New System.Drawing.Size(60, 20)
        Me.LabelRace3.TabIndex = 2
        Me.LabelRace3.Text = "Race 3"
        '
        'LabelRace4
        '
        Me.LabelRace4.AutoSize = True
        Me.LabelRace4.Location = New System.Drawing.Point(477, 88)
        Me.LabelRace4.Name = "LabelRace4"
        Me.LabelRace4.Size = New System.Drawing.Size(60, 20)
        Me.LabelRace4.TabIndex = 3
        Me.LabelRace4.Text = "Race 4"
        '
        'LabelTotal
        '
        Me.LabelTotal.AutoSize = True
        Me.LabelTotal.Location = New System.Drawing.Point(589, 88)
        Me.LabelTotal.Name = "LabelTotal"
        Me.LabelTotal.Size = New System.Drawing.Size(44, 20)
        Me.LabelTotal.TabIndex = 4
        Me.LabelTotal.Text = "Total"
        '
        'LabelBoat1
        '
        Me.LabelBoat1.AutoSize = True
        Me.LabelBoat1.Location = New System.Drawing.Point(17, 146)
        Me.LabelBoat1.Name = "LabelBoat1"
        Me.LabelBoat1.Size = New System.Drawing.Size(69, 20)
        Me.LabelBoat1.TabIndex = 5
        Me.LabelBoat1.Text = "Boat #1:"
        '
        'LabelBoat2
        '
        Me.LabelBoat2.AutoSize = True
        Me.LabelBoat2.Location = New System.Drawing.Point(17, 200)
        Me.LabelBoat2.Name = "LabelBoat2"
        Me.LabelBoat2.Size = New System.Drawing.Size(69, 20)
        Me.LabelBoat2.TabIndex = 6
        Me.LabelBoat2.Text = "Boat #2:"
        '
        'LabelBoat3
        '
        Me.LabelBoat3.AutoSize = True
        Me.LabelBoat3.Location = New System.Drawing.Point(17, 258)
        Me.LabelBoat3.Name = "LabelBoat3"
        Me.LabelBoat3.Size = New System.Drawing.Size(69, 20)
        Me.LabelBoat3.TabIndex = 7
        Me.LabelBoat3.Text = "Boat #3:"
        '
        'TextBoat1Race1
        '
        Me.TextBoat1Race1.Location = New System.Drawing.Point(137, 143)
        Me.TextBoat1Race1.Name = "TextBoat1Race1"
        Me.TextBoat1Race1.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat1Race1.TabIndex = 11
        '
        'TextBoat1Race2
        '
        Me.TextBoat1Race2.Location = New System.Drawing.Point(243, 143)
        Me.TextBoat1Race2.Name = "TextBoat1Race2"
        Me.TextBoat1Race2.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat1Race2.TabIndex = 12
        '
        'TextBoat1Race3
        '
        Me.TextBoat1Race3.Location = New System.Drawing.Point(349, 143)
        Me.TextBoat1Race3.Name = "TextBoat1Race3"
        Me.TextBoat1Race3.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat1Race3.TabIndex = 13
        '
        'TextBoat1Race4
        '
        Me.TextBoat1Race4.Location = New System.Drawing.Point(455, 143)
        Me.TextBoat1Race4.Name = "TextBoat1Race4"
        Me.TextBoat1Race4.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat1Race4.TabIndex = 14
        '
        'TextBoat2Race1
        '
        Me.TextBoat2Race1.Location = New System.Drawing.Point(137, 197)
        Me.TextBoat2Race1.Name = "TextBoat2Race1"
        Me.TextBoat2Race1.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat2Race1.TabIndex = 15
        '
        'TextBoat2Race2
        '
        Me.TextBoat2Race2.Location = New System.Drawing.Point(243, 197)
        Me.TextBoat2Race2.Name = "TextBoat2Race2"
        Me.TextBoat2Race2.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat2Race2.TabIndex = 16
        '
        'TextBoat2Race3
        '
        Me.TextBoat2Race3.Location = New System.Drawing.Point(347, 197)
        Me.TextBoat2Race3.Name = "TextBoat2Race3"
        Me.TextBoat2Race3.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat2Race3.TabIndex = 17
        '
        'TextBoat2Race4
        '
        Me.TextBoat2Race4.Location = New System.Drawing.Point(453, 197)
        Me.TextBoat2Race4.Name = "TextBoat2Race4"
        Me.TextBoat2Race4.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat2Race4.TabIndex = 18
        '
        'TextBoat3Race1
        '
        Me.TextBoat3Race1.Location = New System.Drawing.Point(137, 255)
        Me.TextBoat3Race1.Name = "TextBoat3Race1"
        Me.TextBoat3Race1.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat3Race1.TabIndex = 19
        '
        'TextBoat3Race2
        '
        Me.TextBoat3Race2.Location = New System.Drawing.Point(243, 255)
        Me.TextBoat3Race2.Name = "TextBoat3Race2"
        Me.TextBoat3Race2.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat3Race2.TabIndex = 20
        '
        'TextBoat3Race3
        '
        Me.TextBoat3Race3.Location = New System.Drawing.Point(349, 255)
        Me.TextBoat3Race3.Name = "TextBoat3Race3"
        Me.TextBoat3Race3.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat3Race3.TabIndex = 21
        '
        'TextBoat3Race4
        '
        Me.TextBoat3Race4.Location = New System.Drawing.Point(455, 255)
        Me.TextBoat3Race4.Name = "TextBoat3Race4"
        Me.TextBoat3Race4.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat3Race4.TabIndex = 22
        '
        'ButtonCalculate
        '
        Me.ButtonCalculate.Location = New System.Drawing.Point(21, 330)
        Me.ButtonCalculate.Name = "ButtonCalculate"
        Me.ButtonCalculate.Size = New System.Drawing.Size(154, 44)
        Me.ButtonCalculate.TabIndex = 23
        Me.ButtonCalculate.Text = "Calculate Totals"
        Me.ButtonCalculate.UseVisualStyleBackColor = True
        '
        'ButtonClear
        '
        Me.ButtonClear.Location = New System.Drawing.Point(305, 330)
        Me.ButtonClear.Name = "ButtonClear"
        Me.ButtonClear.Size = New System.Drawing.Size(102, 44)
        Me.ButtonClear.TabIndex = 24
        Me.ButtonClear.Text = "Clear"
        Me.ButtonClear.UseVisualStyleBackColor = True
        '
        'ButtonClose
        '
        Me.ButtonClose.Location = New System.Drawing.Point(557, 330)
        Me.ButtonClose.Name = "ButtonClose"
        Me.ButtonClose.Size = New System.Drawing.Size(102, 44)
        Me.ButtonClose.TabIndex = 25
        Me.ButtonClose.Text = "Close"
        Me.ButtonClose.UseVisualStyleBackColor = True
        '
        'TextBoat1Total
        '
        Me.TextBoat1Total.Enabled = False
        Me.TextBoat1Total.Location = New System.Drawing.Point(559, 143)
        Me.TextBoat1Total.Name = "TextBoat1Total"
        Me.TextBoat1Total.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat1Total.TabIndex = 26
        '
        'TextBoat2Total
        '
        Me.TextBoat2Total.Enabled = False
        Me.TextBoat2Total.Location = New System.Drawing.Point(559, 197)
        Me.TextBoat2Total.Name = "TextBoat2Total"
        Me.TextBoat2Total.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat2Total.TabIndex = 27
        '
        'TextBoat3Total
        '
        Me.TextBoat3Total.Enabled = False
        Me.TextBoat3Total.Location = New System.Drawing.Point(559, 255)
        Me.TextBoat3Total.Name = "TextBoat3Total"
        Me.TextBoat3Total.Size = New System.Drawing.Size(100, 26)
        Me.TextBoat3Total.TabIndex = 28
        '
        'StatusStrip1
        '
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LblStatusErrorMssg})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 402)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(713, 22)
        Me.StatusStrip1.TabIndex = 29
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'LblStatusErrorMssg
        '
        Me.LblStatusErrorMssg.Name = "LblStatusErrorMssg"
        Me.LblStatusErrorMssg.Size = New System.Drawing.Size(0, 17)
        '
        'LabelRaceResults
        '
        Me.LabelRaceResults.AutoSize = True
        Me.LabelRaceResults.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelRaceResults.Location = New System.Drawing.Point(17, 9)
        Me.LabelRaceResults.Name = "LabelRaceResults"
        Me.LabelRaceResults.Size = New System.Drawing.Size(155, 29)
        Me.LabelRaceResults.TabIndex = 30
        Me.LabelRaceResults.Text = "Race Results"
        '
        'FormSailboatRaces
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(713, 424)
        Me.Controls.Add(Me.LabelRaceResults)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.TextBoat3Total)
        Me.Controls.Add(Me.TextBoat2Total)
        Me.Controls.Add(Me.TextBoat1Total)
        Me.Controls.Add(Me.ButtonClose)
        Me.Controls.Add(Me.ButtonClear)
        Me.Controls.Add(Me.ButtonCalculate)
        Me.Controls.Add(Me.TextBoat3Race4)
        Me.Controls.Add(Me.TextBoat3Race3)
        Me.Controls.Add(Me.TextBoat3Race2)
        Me.Controls.Add(Me.TextBoat3Race1)
        Me.Controls.Add(Me.TextBoat2Race4)
        Me.Controls.Add(Me.TextBoat2Race3)
        Me.Controls.Add(Me.TextBoat2Race2)
        Me.Controls.Add(Me.TextBoat2Race1)
        Me.Controls.Add(Me.TextBoat1Race4)
        Me.Controls.Add(Me.TextBoat1Race3)
        Me.Controls.Add(Me.TextBoat1Race2)
        Me.Controls.Add(Me.TextBoat1Race1)
        Me.Controls.Add(Me.LabelBoat3)
        Me.Controls.Add(Me.LabelBoat2)
        Me.Controls.Add(Me.LabelBoat1)
        Me.Controls.Add(Me.LabelTotal)
        Me.Controls.Add(Me.LabelRace4)
        Me.Controls.Add(Me.LabelRace3)
        Me.Controls.Add(Me.LabelRace2)
        Me.Controls.Add(Me.LabelRace1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "FormSailboatRaces"
        Me.Text = "Sailboat Races"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LabelRace1 As Label
    Friend WithEvents LabelRace2 As Label
    Friend WithEvents LabelRace3 As Label
    Friend WithEvents LabelRace4 As Label
    Friend WithEvents LabelTotal As Label
    Friend WithEvents LabelBoat1 As Label
    Friend WithEvents LabelBoat2 As Label
    Friend WithEvents LabelBoat3 As Label
    Friend WithEvents TextBoat1Race1 As TextBox
    Friend WithEvents TextBoat1Race2 As TextBox
    Friend WithEvents TextBoat1Race3 As TextBox
    Friend WithEvents TextBoat1Race4 As TextBox
    Friend WithEvents TextBoat2Race1 As TextBox
    Friend WithEvents TextBoat2Race2 As TextBox
    Friend WithEvents TextBoat2Race3 As TextBox
    Friend WithEvents TextBoat2Race4 As TextBox
    Friend WithEvents TextBoat3Race1 As TextBox
    Friend WithEvents TextBoat3Race2 As TextBox
    Friend WithEvents TextBoat3Race3 As TextBox
    Friend WithEvents TextBoat3Race4 As TextBox
    Friend WithEvents ButtonCalculate As Button
    Friend WithEvents ButtonClear As Button
    Friend WithEvents ButtonClose As Button
    Friend WithEvents TextBoat1Total As TextBox
    Friend WithEvents TextBoat2Total As TextBox
    Friend WithEvents TextBoat3Total As TextBox
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents LblStatusErrorMssg As ToolStripStatusLabel
    Friend WithEvents LabelRaceResults As Label
End Class
