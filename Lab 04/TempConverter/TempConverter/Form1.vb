﻿Public Class FormTempConverter
    Private Sub ButtonClose_Click(sender As Object, e As EventArgs) Handles ButtonClose.Click
        Me.Close()
    End Sub

    Private Sub ButtonClear_Click(sender As Object, e As EventArgs) Handles ButtonClear.Click
        TextCelsiusInput.Clear()
        TextFahrenheitResult.Clear()
    End Sub

    Private Sub ButtonCalculate_Click(sender As Object, e As EventArgs) Handles ButtonCalculate.Click
        Try
            Dim celsius, fahrenheit As Double

            celsius = CDbl(TextCelsiusInput.Text)

            fahrenheit = 1.8 * celsius + 32

            TextFahrenheitResult.Text = fahrenheit
        Catch ex As Exception
            MessageBox.Show("Please enter numbers only.")
        End Try
    End Sub
End Class
