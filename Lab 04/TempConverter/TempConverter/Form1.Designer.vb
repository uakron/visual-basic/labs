﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormTempConverter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelCelsius = New System.Windows.Forms.Label()
        Me.LabelFahrenheit = New System.Windows.Forms.Label()
        Me.TextCelsiusInput = New System.Windows.Forms.TextBox()
        Me.TextFahrenheitResult = New System.Windows.Forms.TextBox()
        Me.ButtonCalculate = New System.Windows.Forms.Button()
        Me.ButtonClear = New System.Windows.Forms.Button()
        Me.ButtonClose = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LabelCelsius
        '
        Me.LabelCelsius.AutoSize = True
        Me.LabelCelsius.Location = New System.Drawing.Point(38, 67)
        Me.LabelCelsius.Name = "LabelCelsius"
        Me.LabelCelsius.Size = New System.Drawing.Size(103, 13)
        Me.LabelCelsius.TabIndex = 0
        Me.LabelCelsius.Text = "Celsius Temperature"
        '
        'LabelFahrenheit
        '
        Me.LabelFahrenheit.AutoSize = True
        Me.LabelFahrenheit.Location = New System.Drawing.Point(38, 134)
        Me.LabelFahrenheit.Name = "LabelFahrenheit"
        Me.LabelFahrenheit.Size = New System.Drawing.Size(120, 13)
        Me.LabelFahrenheit.TabIndex = 1
        Me.LabelFahrenheit.Text = "Fahrenheit Temperature"
        '
        'TextCelsiusInput
        '
        Me.TextCelsiusInput.Location = New System.Drawing.Point(189, 64)
        Me.TextCelsiusInput.Name = "TextCelsiusInput"
        Me.TextCelsiusInput.Size = New System.Drawing.Size(70, 20)
        Me.TextCelsiusInput.TabIndex = 2
        '
        'TextFahrenheitResult
        '
        Me.TextFahrenheitResult.Enabled = False
        Me.TextFahrenheitResult.Location = New System.Drawing.Point(189, 131)
        Me.TextFahrenheitResult.Name = "TextFahrenheitResult"
        Me.TextFahrenheitResult.Size = New System.Drawing.Size(70, 20)
        Me.TextFahrenheitResult.TabIndex = 3
        '
        'ButtonCalculate
        '
        Me.ButtonCalculate.Location = New System.Drawing.Point(23, 266)
        Me.ButtonCalculate.Name = "ButtonCalculate"
        Me.ButtonCalculate.Size = New System.Drawing.Size(75, 23)
        Me.ButtonCalculate.TabIndex = 4
        Me.ButtonCalculate.Text = "Calculate"
        Me.ButtonCalculate.UseVisualStyleBackColor = True
        '
        'ButtonClear
        '
        Me.ButtonClear.Location = New System.Drawing.Point(161, 266)
        Me.ButtonClear.Name = "ButtonClear"
        Me.ButtonClear.Size = New System.Drawing.Size(75, 23)
        Me.ButtonClear.TabIndex = 5
        Me.ButtonClear.Text = "Clear"
        Me.ButtonClear.UseVisualStyleBackColor = True
        '
        'ButtonClose
        '
        Me.ButtonClose.Location = New System.Drawing.Point(289, 266)
        Me.ButtonClose.Name = "ButtonClose"
        Me.ButtonClose.Size = New System.Drawing.Size(75, 23)
        Me.ButtonClose.TabIndex = 6
        Me.ButtonClose.Text = "Exit"
        Me.ButtonClose.UseVisualStyleBackColor = True
        '
        'FormTempConverter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(396, 359)
        Me.Controls.Add(Me.ButtonClose)
        Me.Controls.Add(Me.ButtonClear)
        Me.Controls.Add(Me.ButtonCalculate)
        Me.Controls.Add(Me.TextFahrenheitResult)
        Me.Controls.Add(Me.TextCelsiusInput)
        Me.Controls.Add(Me.LabelFahrenheit)
        Me.Controls.Add(Me.LabelCelsius)
        Me.Name = "FormTempConverter"
        Me.Text = "Celsius to Fahrenheit Converter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LabelCelsius As Label
    Friend WithEvents LabelFahrenheit As Label
    Friend WithEvents TextCelsiusInput As TextBox
    Friend WithEvents TextFahrenheitResult As TextBox
    Friend WithEvents ButtonCalculate As Button
    Friend WithEvents ButtonClear As Button
    Friend WithEvents ButtonClose As Button
End Class
