﻿Public Class FormSumNumbers
    Private Sub BtnExit_Click(sender As Object, e As EventArgs) Handles BtnExit.Click
        Me.Close()
    End Sub

    Private Sub BtnEnterNums_Click(sender As Object, e As EventArgs) Handles BtnEnterNums.Click
        Dim userInput As String
        Dim num As Integer

        userInput = InputBox("Enter a positive integer value.", "Input Needed", 10)

        If Integer.TryParse(userInput, num) AndAlso num > 0 Then
            Dim total As Integer

            For counter As Integer = 1 To num
                total += counter
            Next
            MessageBox.Show("The sum of numbers 1 through " & num & " is " & total)
        Else
            MessageBox.Show("Please enter a positive integer value.")
        End If

    End Sub
End Class
