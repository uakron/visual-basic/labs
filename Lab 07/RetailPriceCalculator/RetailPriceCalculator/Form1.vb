﻿Public Class FormRetailPriceCalc

    Private decWholesale As Decimal
    Private decMarkup As Double

    Function ValidateInput() As Boolean
        If Not Decimal.TryParse(TextWholesale.Text, decWholesale) Or decWholesale < 0 Then
            LblMessage.Text = "Please enter a valid postive number."
            TextWholesale.SelectAll()
            TextWholesale.Focus()
            Return False
        End If

        If Not Double.TryParse(TextMarkup.Text, decMarkup) Or decMarkup < 0 Then
            LblMessage.Text = "Please enter a valid positive number."
            TextMarkup.SelectAll()
            TextMarkup.Focus()
            Return False
        End If

        Return True
    End Function

    Function CalculateMarkupPrice(ByVal decWholesale As Decimal, ByVal decMarkup As Double) As Decimal
        Dim decRetail As Decimal
        decRetail = decWholesale + (decWholesale * (decMarkup / 100))
        Return decRetail
    End Function

    Private Sub BtnExit_Click(sender As Object, e As EventArgs) Handles BtnExit.Click
        Me.Close()
    End Sub

    Private Sub BtnCalculate_Click(sender As Object, e As EventArgs) Handles BtnCalculate.Click
        Dim decRetail As Decimal

        LblMessage.Text = ""

        If ValidateInput() Then
            decRetail = CalculateMarkupPrice(decWholesale, decMarkup)
            TextRetailPrice.Text = decRetail.ToString("c")
        End If
    End Sub
End Class
