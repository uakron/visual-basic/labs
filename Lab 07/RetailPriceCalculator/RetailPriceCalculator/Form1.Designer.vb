﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormRetailPriceCalc
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblWholesale = New System.Windows.Forms.Label()
        Me.LblMarkup = New System.Windows.Forms.Label()
        Me.LblRetailPrice = New System.Windows.Forms.Label()
        Me.TextWholesale = New System.Windows.Forms.TextBox()
        Me.TextMarkup = New System.Windows.Forms.TextBox()
        Me.TextRetailPrice = New System.Windows.Forms.TextBox()
        Me.BtnCalculate = New System.Windows.Forms.Button()
        Me.BtnExit = New System.Windows.Forms.Button()
        Me.LblMessage = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'LblWholesale
        '
        Me.LblWholesale.AutoSize = True
        Me.LblWholesale.Location = New System.Drawing.Point(112, 80)
        Me.LblWholesale.Name = "LblWholesale"
        Me.LblWholesale.Size = New System.Drawing.Size(174, 20)
        Me.LblWholesale.TabIndex = 0
        Me.LblWholesale.Text = "Wholesale Cost of Item"
        '
        'LblMarkup
        '
        Me.LblMarkup.AutoSize = True
        Me.LblMarkup.Location = New System.Drawing.Point(112, 151)
        Me.LblMarkup.Name = "LblMarkup"
        Me.LblMarkup.Size = New System.Drawing.Size(80, 20)
        Me.LblMarkup.TabIndex = 1
        Me.LblMarkup.Text = "Markup %"
        '
        'LblRetailPrice
        '
        Me.LblRetailPrice.AutoSize = True
        Me.LblRetailPrice.Location = New System.Drawing.Point(112, 216)
        Me.LblRetailPrice.Name = "LblRetailPrice"
        Me.LblRetailPrice.Size = New System.Drawing.Size(89, 20)
        Me.LblRetailPrice.TabIndex = 2
        Me.LblRetailPrice.Text = "Retail Price"
        '
        'TextWholesale
        '
        Me.TextWholesale.Location = New System.Drawing.Point(341, 74)
        Me.TextWholesale.Name = "TextWholesale"
        Me.TextWholesale.Size = New System.Drawing.Size(100, 26)
        Me.TextWholesale.TabIndex = 3
        '
        'TextMarkup
        '
        Me.TextMarkup.Location = New System.Drawing.Point(341, 145)
        Me.TextMarkup.Name = "TextMarkup"
        Me.TextMarkup.Size = New System.Drawing.Size(100, 26)
        Me.TextMarkup.TabIndex = 4
        '
        'TextRetailPrice
        '
        Me.TextRetailPrice.Enabled = False
        Me.TextRetailPrice.Location = New System.Drawing.Point(341, 210)
        Me.TextRetailPrice.Name = "TextRetailPrice"
        Me.TextRetailPrice.Size = New System.Drawing.Size(100, 26)
        Me.TextRetailPrice.TabIndex = 5
        '
        'BtnCalculate
        '
        Me.BtnCalculate.Location = New System.Drawing.Point(46, 330)
        Me.BtnCalculate.Name = "BtnCalculate"
        Me.BtnCalculate.Size = New System.Drawing.Size(196, 39)
        Me.BtnCalculate.TabIndex = 6
        Me.BtnCalculate.Text = "Get Retail Price"
        Me.BtnCalculate.UseVisualStyleBackColor = True
        '
        'BtnExit
        '
        Me.BtnExit.Location = New System.Drawing.Point(292, 330)
        Me.BtnExit.Name = "BtnExit"
        Me.BtnExit.Size = New System.Drawing.Size(196, 39)
        Me.BtnExit.TabIndex = 7
        Me.BtnExit.Text = "Exit"
        Me.BtnExit.UseVisualStyleBackColor = True
        '
        'LblMessage
        '
        Me.LblMessage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LblMessage.Location = New System.Drawing.Point(112, 273)
        Me.LblMessage.Name = "LblMessage"
        Me.LblMessage.Size = New System.Drawing.Size(325, 23)
        Me.LblMessage.TabIndex = 8
        Me.LblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FormRetailPriceCalc
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(537, 423)
        Me.Controls.Add(Me.LblMessage)
        Me.Controls.Add(Me.BtnExit)
        Me.Controls.Add(Me.BtnCalculate)
        Me.Controls.Add(Me.TextRetailPrice)
        Me.Controls.Add(Me.TextMarkup)
        Me.Controls.Add(Me.TextWholesale)
        Me.Controls.Add(Me.LblRetailPrice)
        Me.Controls.Add(Me.LblMarkup)
        Me.Controls.Add(Me.LblWholesale)
        Me.Name = "FormRetailPriceCalc"
        Me.Text = "Retail Price Calculator"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LblWholesale As Label
    Friend WithEvents LblMarkup As Label
    Friend WithEvents LblRetailPrice As Label
    Friend WithEvents TextWholesale As TextBox
    Friend WithEvents TextMarkup As TextBox
    Friend WithEvents TextRetailPrice As TextBox
    Friend WithEvents BtnCalculate As Button
    Friend WithEvents BtnExit As Button
    Friend WithEvents LblMessage As Label
End Class
