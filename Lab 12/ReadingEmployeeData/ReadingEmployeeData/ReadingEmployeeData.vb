﻿Imports System.IO
Public Class ReadingEmployeeData
    Dim rcrdNum As Integer = 1
    Dim strFileName As String
    Dim inputFile As StreamReader
    Private Sub BtnExit_Click(sender As Object, e As EventArgs) Handles BtnExit.Click
        Me.Close()
    End Sub

    Private Sub BtnClear_Click(sender As Object, e As EventArgs) Handles BtnClear.Click
        TextRecordNumber.Clear()
        TextFirstName.Clear()
        TextMiddleName.Clear()
        TextLastName.Clear()
        TextEmployeeNumber.Clear()
        TextDepartment.Clear()
        TextTelephone.Clear()
        TextExtension.Clear()
        TextEmail.Clear()
    End Sub

    Private Sub BtnNextRecord_Click(sender As Object, e As EventArgs) Handles BtnNextRecord.Click
        If inputFile.EndOfStream = True Then
            MessageBox.Show("End of File", "Process Complete")
        Else
            TextRecordNumber.Text = CStr(rcrdNum)
            TextFirstName.Text = inputFile.ReadLine
            TextMiddleName.Text = inputFile.ReadLine
            TextLastName.Text = inputFile.ReadLine
            TextEmployeeNumber.Text = inputFile.ReadLine
            TextDepartment.Text = inputFile.ReadLine
            TextTelephone.Text = inputFile.ReadLine
            TextExtension.Text = inputFile.ReadLine
            TextEmail.Text = inputFile.ReadLine
            rcrdNum += 1
            MessageBox.Show("End of File", "Process Copmlete")
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            strFileName = InputBox("Enter the name of the file to open.", "Input Needed")
            inputFile = File.OpenText(strFileName)
        Catch ex As Exception
            strFileName = InputBox("Entered file did not exist; please try another.", "Input Needed")
            inputFile = File.OpenText(strFileName)
        End Try
    End Sub
End Class
