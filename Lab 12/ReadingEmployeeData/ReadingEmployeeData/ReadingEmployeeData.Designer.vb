﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReadingEmployeeData
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnExit = New System.Windows.Forms.Button()
        Me.BtnClear = New System.Windows.Forms.Button()
        Me.BtnNextRecord = New System.Windows.Forms.Button()
        Me.TextEmail = New System.Windows.Forms.TextBox()
        Me.TextExtension = New System.Windows.Forms.TextBox()
        Me.TextTelephone = New System.Windows.Forms.TextBox()
        Me.TextEmployeeNumber = New System.Windows.Forms.TextBox()
        Me.TextLastName = New System.Windows.Forms.TextBox()
        Me.TextMiddleName = New System.Windows.Forms.TextBox()
        Me.TextFirstName = New System.Windows.Forms.TextBox()
        Me.LblEmail = New System.Windows.Forms.Label()
        Me.LblExtension = New System.Windows.Forms.Label()
        Me.LblTelephone = New System.Windows.Forms.Label()
        Me.LblDepartment = New System.Windows.Forms.Label()
        Me.LblEmployeeNumber = New System.Windows.Forms.Label()
        Me.LblLastName = New System.Windows.Forms.Label()
        Me.LblMiddleName = New System.Windows.Forms.Label()
        Me.LblFirstName = New System.Windows.Forms.Label()
        Me.LblEmployeeData = New System.Windows.Forms.Label()
        Me.TextDepartment = New System.Windows.Forms.TextBox()
        Me.LblRecordNumber = New System.Windows.Forms.Label()
        Me.TextRecordNumber = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'BtnExit
        '
        Me.BtnExit.Location = New System.Drawing.Point(245, 359)
        Me.BtnExit.Name = "BtnExit"
        Me.BtnExit.Size = New System.Drawing.Size(105, 49)
        Me.BtnExit.TabIndex = 39
        Me.BtnExit.Text = "Exit"
        Me.BtnExit.UseVisualStyleBackColor = True
        '
        'BtnClear
        '
        Me.BtnClear.Location = New System.Drawing.Point(124, 359)
        Me.BtnClear.Name = "BtnClear"
        Me.BtnClear.Size = New System.Drawing.Size(115, 49)
        Me.BtnClear.TabIndex = 38
        Me.BtnClear.Text = "Clear"
        Me.BtnClear.UseVisualStyleBackColor = True
        '
        'BtnNextRecord
        '
        Me.BtnNextRecord.Location = New System.Drawing.Point(13, 359)
        Me.BtnNextRecord.Name = "BtnNextRecord"
        Me.BtnNextRecord.Size = New System.Drawing.Size(105, 49)
        Me.BtnNextRecord.TabIndex = 37
        Me.BtnNextRecord.Text = "Next Record"
        Me.BtnNextRecord.UseVisualStyleBackColor = True
        '
        'TextEmail
        '
        Me.TextEmail.Enabled = False
        Me.TextEmail.Location = New System.Drawing.Point(166, 288)
        Me.TextEmail.Name = "TextEmail"
        Me.TextEmail.Size = New System.Drawing.Size(100, 20)
        Me.TextEmail.TabIndex = 36
        '
        'TextExtension
        '
        Me.TextExtension.Enabled = False
        Me.TextExtension.Location = New System.Drawing.Point(166, 259)
        Me.TextExtension.Name = "TextExtension"
        Me.TextExtension.Size = New System.Drawing.Size(100, 20)
        Me.TextExtension.TabIndex = 35
        '
        'TextTelephone
        '
        Me.TextTelephone.Enabled = False
        Me.TextTelephone.Location = New System.Drawing.Point(166, 227)
        Me.TextTelephone.Name = "TextTelephone"
        Me.TextTelephone.Size = New System.Drawing.Size(100, 20)
        Me.TextTelephone.TabIndex = 34
        '
        'TextEmployeeNumber
        '
        Me.TextEmployeeNumber.Enabled = False
        Me.TextEmployeeNumber.Location = New System.Drawing.Point(166, 171)
        Me.TextEmployeeNumber.Name = "TextEmployeeNumber"
        Me.TextEmployeeNumber.Size = New System.Drawing.Size(100, 20)
        Me.TextEmployeeNumber.TabIndex = 32
        '
        'TextLastName
        '
        Me.TextLastName.Enabled = False
        Me.TextLastName.Location = New System.Drawing.Point(166, 143)
        Me.TextLastName.Name = "TextLastName"
        Me.TextLastName.Size = New System.Drawing.Size(100, 20)
        Me.TextLastName.TabIndex = 31
        '
        'TextMiddleName
        '
        Me.TextMiddleName.Enabled = False
        Me.TextMiddleName.Location = New System.Drawing.Point(166, 116)
        Me.TextMiddleName.Name = "TextMiddleName"
        Me.TextMiddleName.Size = New System.Drawing.Size(100, 20)
        Me.TextMiddleName.TabIndex = 30
        '
        'TextFirstName
        '
        Me.TextFirstName.Enabled = False
        Me.TextFirstName.Location = New System.Drawing.Point(166, 89)
        Me.TextFirstName.Name = "TextFirstName"
        Me.TextFirstName.Size = New System.Drawing.Size(100, 20)
        Me.TextFirstName.TabIndex = 29
        '
        'LblEmail
        '
        Me.LblEmail.AutoSize = True
        Me.LblEmail.Location = New System.Drawing.Point(71, 291)
        Me.LblEmail.Name = "LblEmail"
        Me.LblEmail.Size = New System.Drawing.Size(79, 13)
        Me.LblEmail.TabIndex = 28
        Me.LblEmail.Text = "E-mail Address:"
        '
        'LblExtension
        '
        Me.LblExtension.AutoSize = True
        Me.LblExtension.Location = New System.Drawing.Point(94, 262)
        Me.LblExtension.Name = "LblExtension"
        Me.LblExtension.Size = New System.Drawing.Size(56, 13)
        Me.LblExtension.TabIndex = 27
        Me.LblExtension.Text = "Extension:"
        '
        'LblTelephone
        '
        Me.LblTelephone.AutoSize = True
        Me.LblTelephone.Location = New System.Drawing.Point(89, 230)
        Me.LblTelephone.Name = "LblTelephone"
        Me.LblTelephone.Size = New System.Drawing.Size(61, 13)
        Me.LblTelephone.TabIndex = 26
        Me.LblTelephone.Text = "Telephone:"
        '
        'LblDepartment
        '
        Me.LblDepartment.AutoSize = True
        Me.LblDepartment.Location = New System.Drawing.Point(85, 200)
        Me.LblDepartment.Name = "LblDepartment"
        Me.LblDepartment.Size = New System.Drawing.Size(65, 13)
        Me.LblDepartment.TabIndex = 25
        Me.LblDepartment.Text = "Department:"
        '
        'LblEmployeeNumber
        '
        Me.LblEmployeeNumber.AutoSize = True
        Me.LblEmployeeNumber.Location = New System.Drawing.Point(54, 174)
        Me.LblEmployeeNumber.Name = "LblEmployeeNumber"
        Me.LblEmployeeNumber.Size = New System.Drawing.Size(96, 13)
        Me.LblEmployeeNumber.TabIndex = 24
        Me.LblEmployeeNumber.Text = "Employee Number:"
        '
        'LblLastName
        '
        Me.LblLastName.AutoSize = True
        Me.LblLastName.Location = New System.Drawing.Point(89, 146)
        Me.LblLastName.Name = "LblLastName"
        Me.LblLastName.Size = New System.Drawing.Size(61, 13)
        Me.LblLastName.TabIndex = 23
        Me.LblLastName.Text = "Last Name:"
        '
        'LblMiddleName
        '
        Me.LblMiddleName.AutoSize = True
        Me.LblMiddleName.Location = New System.Drawing.Point(78, 119)
        Me.LblMiddleName.Name = "LblMiddleName"
        Me.LblMiddleName.Size = New System.Drawing.Size(72, 13)
        Me.LblMiddleName.TabIndex = 22
        Me.LblMiddleName.Text = "Middle Name:"
        '
        'LblFirstName
        '
        Me.LblFirstName.AutoSize = True
        Me.LblFirstName.Location = New System.Drawing.Point(89, 92)
        Me.LblFirstName.Name = "LblFirstName"
        Me.LblFirstName.Size = New System.Drawing.Size(60, 13)
        Me.LblFirstName.TabIndex = 21
        Me.LblFirstName.Text = "First Name:"
        '
        'LblEmployeeData
        '
        Me.LblEmployeeData.AutoSize = True
        Me.LblEmployeeData.Location = New System.Drawing.Point(29, 62)
        Me.LblEmployeeData.Name = "LblEmployeeData"
        Me.LblEmployeeData.Size = New System.Drawing.Size(107, 13)
        Me.LblEmployeeData.TabIndex = 20
        Me.LblEmployeeData.Text = "Enter Employee Data"
        '
        'TextDepartment
        '
        Me.TextDepartment.Enabled = False
        Me.TextDepartment.Location = New System.Drawing.Point(166, 197)
        Me.TextDepartment.Name = "TextDepartment"
        Me.TextDepartment.Size = New System.Drawing.Size(100, 20)
        Me.TextDepartment.TabIndex = 40
        '
        'LblRecordNumber
        '
        Me.LblRecordNumber.AutoSize = True
        Me.LblRecordNumber.Location = New System.Drawing.Point(89, 25)
        Me.LblRecordNumber.Name = "LblRecordNumber"
        Me.LblRecordNumber.Size = New System.Drawing.Size(85, 13)
        Me.LblRecordNumber.TabIndex = 41
        Me.LblRecordNumber.Text = "Record Number:"
        '
        'TextRecordNumber
        '
        Me.TextRecordNumber.Enabled = False
        Me.TextRecordNumber.Location = New System.Drawing.Point(188, 22)
        Me.TextRecordNumber.Name = "TextRecordNumber"
        Me.TextRecordNumber.Size = New System.Drawing.Size(56, 20)
        Me.TextRecordNumber.TabIndex = 42
        '
        'ReadingEmployeeData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(361, 420)
        Me.Controls.Add(Me.TextRecordNumber)
        Me.Controls.Add(Me.LblRecordNumber)
        Me.Controls.Add(Me.TextDepartment)
        Me.Controls.Add(Me.BtnExit)
        Me.Controls.Add(Me.BtnClear)
        Me.Controls.Add(Me.BtnNextRecord)
        Me.Controls.Add(Me.TextEmail)
        Me.Controls.Add(Me.TextExtension)
        Me.Controls.Add(Me.TextTelephone)
        Me.Controls.Add(Me.TextEmployeeNumber)
        Me.Controls.Add(Me.TextLastName)
        Me.Controls.Add(Me.TextMiddleName)
        Me.Controls.Add(Me.TextFirstName)
        Me.Controls.Add(Me.LblEmail)
        Me.Controls.Add(Me.LblExtension)
        Me.Controls.Add(Me.LblTelephone)
        Me.Controls.Add(Me.LblDepartment)
        Me.Controls.Add(Me.LblEmployeeNumber)
        Me.Controls.Add(Me.LblLastName)
        Me.Controls.Add(Me.LblMiddleName)
        Me.Controls.Add(Me.LblFirstName)
        Me.Controls.Add(Me.LblEmployeeData)
        Me.Name = "ReadingEmployeeData"
        Me.Text = "Employee Data"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BtnExit As Button
    Friend WithEvents BtnClear As Button
    Friend WithEvents BtnNextRecord As Button
    Friend WithEvents TextEmail As TextBox
    Friend WithEvents TextExtension As TextBox
    Friend WithEvents TextTelephone As TextBox
    Friend WithEvents TextEmployeeNumber As TextBox
    Friend WithEvents TextLastName As TextBox
    Friend WithEvents TextMiddleName As TextBox
    Friend WithEvents TextFirstName As TextBox
    Friend WithEvents LblEmail As Label
    Friend WithEvents LblExtension As Label
    Friend WithEvents LblTelephone As Label
    Friend WithEvents LblDepartment As Label
    Friend WithEvents LblEmployeeNumber As Label
    Friend WithEvents LblLastName As Label
    Friend WithEvents LblMiddleName As Label
    Friend WithEvents LblFirstName As Label
    Friend WithEvents LblEmployeeData As Label
    Friend WithEvents TextDepartment As TextBox
    Friend WithEvents LblRecordNumber As Label
    Friend WithEvents TextRecordNumber As TextBox
End Class
