﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CreatingEmployeeData
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblEmployeeData = New System.Windows.Forms.Label()
        Me.LblFirstName = New System.Windows.Forms.Label()
        Me.LblMiddleName = New System.Windows.Forms.Label()
        Me.LblLastName = New System.Windows.Forms.Label()
        Me.LblEmployeeNumber = New System.Windows.Forms.Label()
        Me.LblDepartment = New System.Windows.Forms.Label()
        Me.LblTelephone = New System.Windows.Forms.Label()
        Me.LblExtension = New System.Windows.Forms.Label()
        Me.LblEmail = New System.Windows.Forms.Label()
        Me.TextFirstName = New System.Windows.Forms.TextBox()
        Me.TextMiddleName = New System.Windows.Forms.TextBox()
        Me.TextLastName = New System.Windows.Forms.TextBox()
        Me.TextEmployeeNumber = New System.Windows.Forms.TextBox()
        Me.ComboBoxDepartment = New System.Windows.Forms.ComboBox()
        Me.TextTelephone = New System.Windows.Forms.TextBox()
        Me.TextExtension = New System.Windows.Forms.TextBox()
        Me.TextEmail = New System.Windows.Forms.TextBox()
        Me.BtnSave = New System.Windows.Forms.Button()
        Me.BtnClear = New System.Windows.Forms.Button()
        Me.BtnExit = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LblEmployeeData
        '
        Me.LblEmployeeData.AutoSize = True
        Me.LblEmployeeData.Location = New System.Drawing.Point(28, 31)
        Me.LblEmployeeData.Name = "LblEmployeeData"
        Me.LblEmployeeData.Size = New System.Drawing.Size(107, 13)
        Me.LblEmployeeData.TabIndex = 0
        Me.LblEmployeeData.Text = "Enter Employee Data"
        '
        'LblFirstName
        '
        Me.LblFirstName.AutoSize = True
        Me.LblFirstName.Location = New System.Drawing.Point(88, 61)
        Me.LblFirstName.Name = "LblFirstName"
        Me.LblFirstName.Size = New System.Drawing.Size(60, 13)
        Me.LblFirstName.TabIndex = 1
        Me.LblFirstName.Text = "First Name:"
        '
        'LblMiddleName
        '
        Me.LblMiddleName.AutoSize = True
        Me.LblMiddleName.Location = New System.Drawing.Point(77, 88)
        Me.LblMiddleName.Name = "LblMiddleName"
        Me.LblMiddleName.Size = New System.Drawing.Size(72, 13)
        Me.LblMiddleName.TabIndex = 2
        Me.LblMiddleName.Text = "Middle Name:"
        '
        'LblLastName
        '
        Me.LblLastName.AutoSize = True
        Me.LblLastName.Location = New System.Drawing.Point(88, 115)
        Me.LblLastName.Name = "LblLastName"
        Me.LblLastName.Size = New System.Drawing.Size(61, 13)
        Me.LblLastName.TabIndex = 3
        Me.LblLastName.Text = "Last Name:"
        '
        'LblEmployeeNumber
        '
        Me.LblEmployeeNumber.AutoSize = True
        Me.LblEmployeeNumber.Location = New System.Drawing.Point(53, 143)
        Me.LblEmployeeNumber.Name = "LblEmployeeNumber"
        Me.LblEmployeeNumber.Size = New System.Drawing.Size(96, 13)
        Me.LblEmployeeNumber.TabIndex = 4
        Me.LblEmployeeNumber.Text = "Employee Number:"
        '
        'LblDepartment
        '
        Me.LblDepartment.AutoSize = True
        Me.LblDepartment.Location = New System.Drawing.Point(84, 169)
        Me.LblDepartment.Name = "LblDepartment"
        Me.LblDepartment.Size = New System.Drawing.Size(65, 13)
        Me.LblDepartment.TabIndex = 5
        Me.LblDepartment.Text = "Department:"
        '
        'LblTelephone
        '
        Me.LblTelephone.AutoSize = True
        Me.LblTelephone.Location = New System.Drawing.Point(88, 199)
        Me.LblTelephone.Name = "LblTelephone"
        Me.LblTelephone.Size = New System.Drawing.Size(61, 13)
        Me.LblTelephone.TabIndex = 6
        Me.LblTelephone.Text = "Telephone:"
        '
        'LblExtension
        '
        Me.LblExtension.AutoSize = True
        Me.LblExtension.Location = New System.Drawing.Point(93, 231)
        Me.LblExtension.Name = "LblExtension"
        Me.LblExtension.Size = New System.Drawing.Size(56, 13)
        Me.LblExtension.TabIndex = 7
        Me.LblExtension.Text = "Extension:"
        '
        'LblEmail
        '
        Me.LblEmail.AutoSize = True
        Me.LblEmail.Location = New System.Drawing.Point(70, 260)
        Me.LblEmail.Name = "LblEmail"
        Me.LblEmail.Size = New System.Drawing.Size(79, 13)
        Me.LblEmail.TabIndex = 8
        Me.LblEmail.Text = "E-mail Address:"
        '
        'TextFirstName
        '
        Me.TextFirstName.Location = New System.Drawing.Point(165, 58)
        Me.TextFirstName.Name = "TextFirstName"
        Me.TextFirstName.Size = New System.Drawing.Size(100, 20)
        Me.TextFirstName.TabIndex = 9
        '
        'TextMiddleName
        '
        Me.TextMiddleName.Location = New System.Drawing.Point(165, 85)
        Me.TextMiddleName.Name = "TextMiddleName"
        Me.TextMiddleName.Size = New System.Drawing.Size(100, 20)
        Me.TextMiddleName.TabIndex = 10
        '
        'TextLastName
        '
        Me.TextLastName.Location = New System.Drawing.Point(165, 112)
        Me.TextLastName.Name = "TextLastName"
        Me.TextLastName.Size = New System.Drawing.Size(100, 20)
        Me.TextLastName.TabIndex = 11
        '
        'TextEmployeeNumber
        '
        Me.TextEmployeeNumber.Location = New System.Drawing.Point(165, 140)
        Me.TextEmployeeNumber.Name = "TextEmployeeNumber"
        Me.TextEmployeeNumber.Size = New System.Drawing.Size(100, 20)
        Me.TextEmployeeNumber.TabIndex = 12
        '
        'ComboBoxDepartment
        '
        Me.ComboBoxDepartment.FormattingEnabled = True
        Me.ComboBoxDepartment.Items.AddRange(New Object() {"Accounting", "Administration", "Marketing", "MIS", "Sales"})
        Me.ComboBoxDepartment.Location = New System.Drawing.Point(165, 166)
        Me.ComboBoxDepartment.Name = "ComboBoxDepartment"
        Me.ComboBoxDepartment.Size = New System.Drawing.Size(100, 21)
        Me.ComboBoxDepartment.TabIndex = 13
        '
        'TextTelephone
        '
        Me.TextTelephone.Location = New System.Drawing.Point(165, 196)
        Me.TextTelephone.Name = "TextTelephone"
        Me.TextTelephone.Size = New System.Drawing.Size(100, 20)
        Me.TextTelephone.TabIndex = 14
        '
        'TextExtension
        '
        Me.TextExtension.Location = New System.Drawing.Point(165, 228)
        Me.TextExtension.Name = "TextExtension"
        Me.TextExtension.Size = New System.Drawing.Size(100, 20)
        Me.TextExtension.TabIndex = 15
        '
        'TextEmail
        '
        Me.TextEmail.Location = New System.Drawing.Point(165, 257)
        Me.TextEmail.Name = "TextEmail"
        Me.TextEmail.Size = New System.Drawing.Size(100, 20)
        Me.TextEmail.TabIndex = 16
        '
        'BtnSave
        '
        Me.BtnSave.Location = New System.Drawing.Point(12, 328)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Size = New System.Drawing.Size(105, 49)
        Me.BtnSave.TabIndex = 17
        Me.BtnSave.Text = "Save Record"
        Me.BtnSave.UseVisualStyleBackColor = True
        '
        'BtnClear
        '
        Me.BtnClear.Location = New System.Drawing.Point(123, 328)
        Me.BtnClear.Name = "BtnClear"
        Me.BtnClear.Size = New System.Drawing.Size(115, 49)
        Me.BtnClear.TabIndex = 18
        Me.BtnClear.Text = "Clear"
        Me.BtnClear.UseVisualStyleBackColor = True
        '
        'BtnExit
        '
        Me.BtnExit.Location = New System.Drawing.Point(244, 328)
        Me.BtnExit.Name = "BtnExit"
        Me.BtnExit.Size = New System.Drawing.Size(105, 49)
        Me.BtnExit.TabIndex = 19
        Me.BtnExit.Text = "Exit"
        Me.BtnExit.UseVisualStyleBackColor = True
        '
        'CreatingEmployeeData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(361, 420)
        Me.Controls.Add(Me.BtnExit)
        Me.Controls.Add(Me.BtnClear)
        Me.Controls.Add(Me.BtnSave)
        Me.Controls.Add(Me.TextEmail)
        Me.Controls.Add(Me.TextExtension)
        Me.Controls.Add(Me.TextTelephone)
        Me.Controls.Add(Me.ComboBoxDepartment)
        Me.Controls.Add(Me.TextEmployeeNumber)
        Me.Controls.Add(Me.TextLastName)
        Me.Controls.Add(Me.TextMiddleName)
        Me.Controls.Add(Me.TextFirstName)
        Me.Controls.Add(Me.LblEmail)
        Me.Controls.Add(Me.LblExtension)
        Me.Controls.Add(Me.LblTelephone)
        Me.Controls.Add(Me.LblDepartment)
        Me.Controls.Add(Me.LblEmployeeNumber)
        Me.Controls.Add(Me.LblLastName)
        Me.Controls.Add(Me.LblMiddleName)
        Me.Controls.Add(Me.LblFirstName)
        Me.Controls.Add(Me.LblEmployeeData)
        Me.Name = "CreatingEmployeeData"
        Me.Text = "Employee Data"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LblEmployeeData As Label
    Friend WithEvents LblFirstName As Label
    Friend WithEvents LblMiddleName As Label
    Friend WithEvents LblLastName As Label
    Friend WithEvents LblEmployeeNumber As Label
    Friend WithEvents LblDepartment As Label
    Friend WithEvents LblTelephone As Label
    Friend WithEvents LblExtension As Label
    Friend WithEvents LblEmail As Label
    Friend WithEvents TextFirstName As TextBox
    Friend WithEvents TextMiddleName As TextBox
    Friend WithEvents TextLastName As TextBox
    Friend WithEvents TextEmployeeNumber As TextBox
    Friend WithEvents ComboBoxDepartment As ComboBox
    Friend WithEvents TextTelephone As TextBox
    Friend WithEvents TextExtension As TextBox
    Friend WithEvents TextEmail As TextBox
    Friend WithEvents BtnSave As Button
    Friend WithEvents BtnClear As Button
    Friend WithEvents BtnExit As Button
End Class
