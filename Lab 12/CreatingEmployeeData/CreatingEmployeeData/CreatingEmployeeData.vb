﻿Imports System.IO
Public Class CreatingEmployeeData
    Dim strFileName As String

    Private Sub CreatingEmployeeData_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        strFileName = InputBox("Enter the filename.", "Input Needed")
    End Sub
    Private Sub BtnExit_Click(sender As Object, e As EventArgs) Handles BtnExit.Click
        Me.Close()
    End Sub
    Private Sub BtnClear_Click(sender As Object, e As EventArgs) Handles BtnClear.Click
        TextFirstName.Clear()
        TextMiddleName.Clear()
        TextLastName.Clear()
        TextEmployeeNumber.Clear()
        ComboBoxDepartment.SelectedIndex = -1
        TextTelephone.Clear()
        TextExtension.Clear()
        TextEmail.Clear()
    End Sub
    Private Sub BtnSave_Click(sender As Object, e As EventArgs) Handles BtnSave.Click
        Dim employeeFile As StreamWriter
        Dim strFirstName As String = TextFirstName.Text
        Dim strMiddleNmae As String = TextMiddleName.Text
        Dim strLastName As String = TextLastName.Text
        Dim strEmployeeNum As String = TextEmployeeNumber.Text
        Dim strDepartment As String = ComboBoxDepartment.Text
        Dim strTelephone As String = TextTelephone.Text
        Dim strExtension As String = TextExtension.Text
        Dim strEmail As String = TextEmail.Text

        Try
            employeeFile = File.CreateText(strFileName)

            employeeFile.WriteLine(strFirstName)
            employeeFile.WriteLine(strMiddleNmae)
            employeeFile.WriteLine(strLastName)
            employeeFile.WriteLine(strEmployeeNum)
            employeeFile.WriteLine(strDepartment)
            employeeFile.WriteLine(strTelephone)
            employeeFile.WriteLine(strExtension)
            employeeFile.WriteLine(strEmail)

            employeeFile.Close()

            MessageBox.Show("Employee Data saved to file", "Process Complete")
        Catch ex As Exception
            employeeFile = File.AppendText(strFileName)

            employeeFile.WriteLine(strFirstName)
            employeeFile.WriteLine(strMiddleNmae)
            employeeFile.WriteLine(strLastName)
            employeeFile.WriteLine(strEmployeeNum)
            employeeFile.WriteLine(strDepartment)
            employeeFile.WriteLine(strTelephone)
            employeeFile.WriteLine(strExtension)
            employeeFile.WriteLine(strEmail)

            employeeFile.Close()

            MessageBox.Show("Employee Data appended to file", "Process Complete")
        End Try
    End Sub
End Class