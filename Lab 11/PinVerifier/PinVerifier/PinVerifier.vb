﻿Public Class PinVerifier
    Private Sub BtnExit_Click(sender As Object, e As EventArgs) Handles BtnExit.Click
        Me.Close()
    End Sub

    Private Sub BtnClear_Click(sender As Object, e As EventArgs) Handles BtnClear.Click
        TextPIN1.Clear()
        TextPIN2.Clear()
        TextPIN3.Clear()
        TextPIN4.Clear()
        TextPIN5.Clear()
        TextPIN6.Clear()
        TextPIN7.Clear()
    End Sub

    Private Sub BtnVerify_Click(sender As Object, e As EventArgs) Handles BtnVerify.Click
        Dim intMinimum() As Integer = {7, 5, 0, 0, 6, 3, 4}
        Dim intMaximum() As Integer = {9, 7, 4, 9, 9, 6, 8}
        Dim pin1, pin2, pin3, pin4, pin5, pin6, pin7 As Integer
        Dim blnVerify As Boolean = True

        If Not Integer.TryParse(TextPIN1.Text, pin1) Then
            MessageBox.Show("Digit 1: Please enter an integer", "Invalid")
            blnVerify = False
        ElseIf pin1 < intMinimum(0) Or pin1 > intMaximum(0) Then
            MessageBox.Show("Digit 1: Must be in the range of 7 through 9", "Invalid")
            blnVerify = False
        End If

        If Not Integer.TryParse(TextPIN2.Text, pin2) Then
            MessageBox.Show("Digit 2: Please enter an integer", "Invalid")
            blnVerify = False
        ElseIf pin2 < intMinimum(1) Or pin2 > intMaximum(1) Then
            MessageBox.Show("Digit 2: Must be in the range of 5 through 7", "Invalid")
            blnVerify = False
        End If

        If Not Integer.TryParse(TextPIN3.Text, pin3) Then
            MessageBox.Show("Digit 3: Please enter an integer", "Invalid")
            blnVerify = False
        ElseIf pin3 < intMinimum(2) Or pin3 > intMaximum(2) Then
            MessageBox.Show("Digit 3: Must be in the range of 0 through 4", "Invalid")
            blnVerify = False
        End If

        If Not Integer.TryParse(TextPIN4.Text, pin4) Then
            MessageBox.Show("Digit 4: Please enter an integer", "Invalid")
            blnVerify = False
        ElseIf pin4 < intMinimum(3) Or pin4 > intMaximum(3) Then
            MessageBox.Show("Digit 4: Must be in the range of 0 through 9", "Invalid")
            blnVerify = False
        End If

        If Not Integer.TryParse(TextPIN5.Text, pin5) Then
            MessageBox.Show("Digit 5: Please enter an integer", "Invalid")
            blnVerify = False
        ElseIf pin5 < intMinimum(4) Or pin5 > intMaximum(4) Then
            MessageBox.Show("Digit 5: Must be in the range of 6 through 9", "Invalid")
            blnVerify = False
        End If

        If Not Integer.TryParse(TextPIN6.Text, pin6) Then
            MessageBox.Show("Digit 6: Please enter an integer", "Invalid")
            blnVerify = False
        ElseIf pin6 < intMinimum(5) Or pin6 > intMaximum(5) Then
            MessageBox.Show("Digit 6: Must be in the range of 3 through 6", "Invalid")
            blnVerify = False
        End If

        If Not Integer.TryParse(TextPIN7.Text, pin7) Then
            MessageBox.Show("Digit 7: Please enter an integer", "Invalid")
            blnVerify = False
        ElseIf pin7 < intMinimum(6) Or pin7 > intMaximum(6) Then
            MessageBox.Show("Digit 7: Must be in the range of 4 through 8", "Invalid")
            blnVerify = False
        End If

        If blnVerify = True Then
            MessageBox.Show("PIN Verified", "Valid")
        End If
    End Sub
End Class
