﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PinVerifier
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblEnter = New System.Windows.Forms.Label()
        Me.TextPIN1 = New System.Windows.Forms.TextBox()
        Me.TextPIN2 = New System.Windows.Forms.TextBox()
        Me.TextPIN3 = New System.Windows.Forms.TextBox()
        Me.TextPIN4 = New System.Windows.Forms.TextBox()
        Me.TextPIN5 = New System.Windows.Forms.TextBox()
        Me.TextPIN6 = New System.Windows.Forms.TextBox()
        Me.TextPIN7 = New System.Windows.Forms.TextBox()
        Me.BtnVerify = New System.Windows.Forms.Button()
        Me.BtnClear = New System.Windows.Forms.Button()
        Me.BtnExit = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LblEnter
        '
        Me.LblEnter.AutoSize = True
        Me.LblEnter.Location = New System.Drawing.Point(33, 31)
        Me.LblEnter.Name = "LblEnter"
        Me.LblEnter.Size = New System.Drawing.Size(71, 13)
        Me.LblEnter.TabIndex = 0
        Me.LblEnter.Text = "Enter the PIN"
        '
        'TextPIN1
        '
        Me.TextPIN1.Location = New System.Drawing.Point(38, 61)
        Me.TextPIN1.Name = "TextPIN1"
        Me.TextPIN1.Size = New System.Drawing.Size(30, 20)
        Me.TextPIN1.TabIndex = 1
        '
        'TextPIN2
        '
        Me.TextPIN2.Location = New System.Drawing.Point(74, 61)
        Me.TextPIN2.Name = "TextPIN2"
        Me.TextPIN2.Size = New System.Drawing.Size(30, 20)
        Me.TextPIN2.TabIndex = 2
        '
        'TextPIN3
        '
        Me.TextPIN3.Location = New System.Drawing.Point(110, 61)
        Me.TextPIN3.Name = "TextPIN3"
        Me.TextPIN3.Size = New System.Drawing.Size(30, 20)
        Me.TextPIN3.TabIndex = 3
        '
        'TextPIN4
        '
        Me.TextPIN4.Location = New System.Drawing.Point(146, 61)
        Me.TextPIN4.Name = "TextPIN4"
        Me.TextPIN4.Size = New System.Drawing.Size(30, 20)
        Me.TextPIN4.TabIndex = 4
        '
        'TextPIN5
        '
        Me.TextPIN5.Location = New System.Drawing.Point(182, 61)
        Me.TextPIN5.Name = "TextPIN5"
        Me.TextPIN5.Size = New System.Drawing.Size(30, 20)
        Me.TextPIN5.TabIndex = 5
        '
        'TextPIN6
        '
        Me.TextPIN6.Location = New System.Drawing.Point(218, 61)
        Me.TextPIN6.Name = "TextPIN6"
        Me.TextPIN6.Size = New System.Drawing.Size(30, 20)
        Me.TextPIN6.TabIndex = 6
        '
        'TextPIN7
        '
        Me.TextPIN7.Location = New System.Drawing.Point(254, 61)
        Me.TextPIN7.Name = "TextPIN7"
        Me.TextPIN7.Size = New System.Drawing.Size(30, 20)
        Me.TextPIN7.TabIndex = 7
        '
        'BtnVerify
        '
        Me.BtnVerify.Location = New System.Drawing.Point(13, 103)
        Me.BtnVerify.Name = "BtnVerify"
        Me.BtnVerify.Size = New System.Drawing.Size(91, 47)
        Me.BtnVerify.TabIndex = 8
        Me.BtnVerify.Text = "Verify"
        Me.BtnVerify.UseVisualStyleBackColor = True
        '
        'BtnClear
        '
        Me.BtnClear.Location = New System.Drawing.Point(110, 103)
        Me.BtnClear.Name = "BtnClear"
        Me.BtnClear.Size = New System.Drawing.Size(91, 47)
        Me.BtnClear.TabIndex = 9
        Me.BtnClear.Text = "Clear"
        Me.BtnClear.UseVisualStyleBackColor = True
        '
        'BtnExit
        '
        Me.BtnExit.Location = New System.Drawing.Point(208, 103)
        Me.BtnExit.Name = "BtnExit"
        Me.BtnExit.Size = New System.Drawing.Size(91, 47)
        Me.BtnExit.TabIndex = 10
        Me.BtnExit.Text = "Exit"
        Me.BtnExit.UseVisualStyleBackColor = True
        '
        'PinVerifier
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(324, 162)
        Me.Controls.Add(Me.BtnExit)
        Me.Controls.Add(Me.BtnClear)
        Me.Controls.Add(Me.BtnVerify)
        Me.Controls.Add(Me.TextPIN7)
        Me.Controls.Add(Me.TextPIN6)
        Me.Controls.Add(Me.TextPIN5)
        Me.Controls.Add(Me.TextPIN4)
        Me.Controls.Add(Me.TextPIN3)
        Me.Controls.Add(Me.TextPIN2)
        Me.Controls.Add(Me.TextPIN1)
        Me.Controls.Add(Me.LblEnter)
        Me.Name = "PinVerifier"
        Me.Text = "Pin Verifier"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LblEnter As Label
    Friend WithEvents TextPIN1 As TextBox
    Friend WithEvents TextPIN2 As TextBox
    Friend WithEvents TextPIN3 As TextBox
    Friend WithEvents TextPIN4 As TextBox
    Friend WithEvents TextPIN5 As TextBox
    Friend WithEvents TextPIN6 As TextBox
    Friend WithEvents TextPIN7 As TextBox
    Friend WithEvents BtnVerify As Button
    Friend WithEvents BtnClear As Button
    Friend WithEvents BtnExit As Button
End Class
