﻿Public Class FormWeeklyTempAvg
    Private Sub ButtonCalculate_Click(sender As Object, e As EventArgs) Handles ButtonCalculate.Click
        Try
            Dim tempWeek1, tempWeek2, tempWeek3, tempWeek4, tempWeek5 As Double
            Dim tempAverage As Double

            tempWeek1 = Convert.ToDouble(TextBoxWeek1.Text)
            tempWeek2 = Convert.ToDouble(TextBoxWeek2.Text)
            tempWeek3 = Convert.ToDouble(TextBoxWeek3.Text)
            tempWeek4 = Convert.ToDouble(TextBoxWeek4.Text)
            tempWeek5 = Convert.ToDouble(TextBoxWeek5.Text)

            tempAverage = (tempWeek1 + tempWeek2 + tempWeek3 + tempWeek4 + tempWeek5) / 5

            TextBoxAverageCalc.Text = tempAverage



        Catch ex As Exception
            MessageBox.Show("Please enter numbers only.")
        End Try

    End Sub

    Private Sub ButtonClear_Click(sender As Object, e As EventArgs) Handles ButtonClear.Click
        TextBoxWeek1.Clear()
        TextBoxWeek2.Clear()
        TextBoxWeek3.Clear()
        TextBoxWeek4.Clear()
        TextBoxWeek5.Clear()
        TextBoxAverageCalc.Clear()
    End Sub

    Private Sub ButtonExit_Click(sender As Object, e As EventArgs) Handles ButtonExit.Click
        Me.Close()
    End Sub
End Class
