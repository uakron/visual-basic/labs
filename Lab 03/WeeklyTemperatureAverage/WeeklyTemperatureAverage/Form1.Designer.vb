﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormWeeklyTempAvg
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextBoxWeek1 = New System.Windows.Forms.TextBox()
        Me.TextBoxWeek2 = New System.Windows.Forms.TextBox()
        Me.TextBoxWeek3 = New System.Windows.Forms.TextBox()
        Me.TextBoxWeek4 = New System.Windows.Forms.TextBox()
        Me.LabelWeek1 = New System.Windows.Forms.Label()
        Me.LabelWeek2 = New System.Windows.Forms.Label()
        Me.LabelWeek3 = New System.Windows.Forms.Label()
        Me.LabelWeek4 = New System.Windows.Forms.Label()
        Me.LabelWeek5 = New System.Windows.Forms.Label()
        Me.ButtonCalculate = New System.Windows.Forms.Button()
        Me.ButtonClear = New System.Windows.Forms.Button()
        Me.ButtonExit = New System.Windows.Forms.Button()
        Me.TextBoxWeek5 = New System.Windows.Forms.TextBox()
        Me.LabelAverage = New System.Windows.Forms.Label()
        Me.TextBoxAverageCalc = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'TextBoxWeek1
        '
        Me.TextBoxWeek1.Location = New System.Drawing.Point(165, 48)
        Me.TextBoxWeek1.Name = "TextBoxWeek1"
        Me.TextBoxWeek1.Size = New System.Drawing.Size(100, 26)
        Me.TextBoxWeek1.TabIndex = 0
        '
        'TextBoxWeek2
        '
        Me.TextBoxWeek2.Location = New System.Drawing.Point(165, 80)
        Me.TextBoxWeek2.Name = "TextBoxWeek2"
        Me.TextBoxWeek2.Size = New System.Drawing.Size(100, 26)
        Me.TextBoxWeek2.TabIndex = 1
        '
        'TextBoxWeek3
        '
        Me.TextBoxWeek3.Location = New System.Drawing.Point(165, 112)
        Me.TextBoxWeek3.Name = "TextBoxWeek3"
        Me.TextBoxWeek3.Size = New System.Drawing.Size(100, 26)
        Me.TextBoxWeek3.TabIndex = 2
        '
        'TextBoxWeek4
        '
        Me.TextBoxWeek4.Location = New System.Drawing.Point(165, 144)
        Me.TextBoxWeek4.Name = "TextBoxWeek4"
        Me.TextBoxWeek4.Size = New System.Drawing.Size(100, 26)
        Me.TextBoxWeek4.TabIndex = 3
        '
        'LabelWeek1
        '
        Me.LabelWeek1.AutoSize = True
        Me.LabelWeek1.Location = New System.Drawing.Point(84, 51)
        Me.LabelWeek1.Name = "LabelWeek1"
        Me.LabelWeek1.Size = New System.Drawing.Size(63, 20)
        Me.LabelWeek1.TabIndex = 9
        Me.LabelWeek1.Text = "Week 1"
        Me.LabelWeek1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelWeek2
        '
        Me.LabelWeek2.AutoSize = True
        Me.LabelWeek2.Location = New System.Drawing.Point(84, 83)
        Me.LabelWeek2.Name = "LabelWeek2"
        Me.LabelWeek2.Size = New System.Drawing.Size(63, 20)
        Me.LabelWeek2.TabIndex = 10
        Me.LabelWeek2.Text = "Week 2"
        Me.LabelWeek2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelWeek3
        '
        Me.LabelWeek3.AutoSize = True
        Me.LabelWeek3.Location = New System.Drawing.Point(84, 115)
        Me.LabelWeek3.Name = "LabelWeek3"
        Me.LabelWeek3.Size = New System.Drawing.Size(63, 20)
        Me.LabelWeek3.TabIndex = 11
        Me.LabelWeek3.Text = "Week 3"
        Me.LabelWeek3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelWeek4
        '
        Me.LabelWeek4.AutoSize = True
        Me.LabelWeek4.Location = New System.Drawing.Point(84, 147)
        Me.LabelWeek4.Name = "LabelWeek4"
        Me.LabelWeek4.Size = New System.Drawing.Size(63, 20)
        Me.LabelWeek4.TabIndex = 12
        Me.LabelWeek4.Text = "Week 4"
        Me.LabelWeek4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelWeek5
        '
        Me.LabelWeek5.AutoSize = True
        Me.LabelWeek5.Location = New System.Drawing.Point(84, 179)
        Me.LabelWeek5.Name = "LabelWeek5"
        Me.LabelWeek5.Size = New System.Drawing.Size(63, 20)
        Me.LabelWeek5.TabIndex = 13
        Me.LabelWeek5.Text = "Week 5"
        Me.LabelWeek5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonCalculate
        '
        Me.ButtonCalculate.Location = New System.Drawing.Point(26, 279)
        Me.ButtonCalculate.Name = "ButtonCalculate"
        Me.ButtonCalculate.Size = New System.Drawing.Size(103, 35)
        Me.ButtonCalculate.TabIndex = 6
        Me.ButtonCalculate.Text = "Calculate"
        Me.ButtonCalculate.UseVisualStyleBackColor = True
        '
        'ButtonClear
        '
        Me.ButtonClear.Location = New System.Drawing.Point(164, 279)
        Me.ButtonClear.Name = "ButtonClear"
        Me.ButtonClear.Size = New System.Drawing.Size(103, 35)
        Me.ButtonClear.TabIndex = 7
        Me.ButtonClear.Text = "Clear"
        Me.ButtonClear.UseVisualStyleBackColor = True
        '
        'ButtonExit
        '
        Me.ButtonExit.Location = New System.Drawing.Point(301, 279)
        Me.ButtonExit.Name = "ButtonExit"
        Me.ButtonExit.Size = New System.Drawing.Size(103, 35)
        Me.ButtonExit.TabIndex = 8
        Me.ButtonExit.Text = "Exit"
        Me.ButtonExit.UseVisualStyleBackColor = True
        '
        'TextBoxWeek5
        '
        Me.TextBoxWeek5.Location = New System.Drawing.Point(165, 176)
        Me.TextBoxWeek5.Name = "TextBoxWeek5"
        Me.TextBoxWeek5.Size = New System.Drawing.Size(100, 26)
        Me.TextBoxWeek5.TabIndex = 5
        '
        'LabelAverage
        '
        Me.LabelAverage.AutoSize = True
        Me.LabelAverage.Location = New System.Drawing.Point(84, 211)
        Me.LabelAverage.Name = "LabelAverage"
        Me.LabelAverage.Size = New System.Drawing.Size(68, 20)
        Me.LabelAverage.TabIndex = 14
        Me.LabelAverage.Text = "Average"
        Me.LabelAverage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBoxAverageCalc
        '
        Me.TextBoxAverageCalc.Enabled = False
        Me.TextBoxAverageCalc.Location = New System.Drawing.Point(165, 208)
        Me.TextBoxAverageCalc.Name = "TextBoxAverageCalc"
        Me.TextBoxAverageCalc.Size = New System.Drawing.Size(100, 26)
        Me.TextBoxAverageCalc.TabIndex = 15
        '
        'FormWeeklyTempAvg
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(430, 381)
        Me.Controls.Add(Me.TextBoxAverageCalc)
        Me.Controls.Add(Me.LabelAverage)
        Me.Controls.Add(Me.TextBoxWeek5)
        Me.Controls.Add(Me.ButtonExit)
        Me.Controls.Add(Me.ButtonClear)
        Me.Controls.Add(Me.ButtonCalculate)
        Me.Controls.Add(Me.LabelWeek5)
        Me.Controls.Add(Me.LabelWeek4)
        Me.Controls.Add(Me.LabelWeek3)
        Me.Controls.Add(Me.LabelWeek2)
        Me.Controls.Add(Me.LabelWeek1)
        Me.Controls.Add(Me.TextBoxWeek4)
        Me.Controls.Add(Me.TextBoxWeek3)
        Me.Controls.Add(Me.TextBoxWeek2)
        Me.Controls.Add(Me.TextBoxWeek1)
        Me.Name = "FormWeeklyTempAvg"
        Me.Text = "Weekly Temperature Average"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBoxWeek1 As TextBox
    Friend WithEvents TextBoxWeek2 As TextBox
    Friend WithEvents TextBoxWeek3 As TextBox
    Friend WithEvents TextBoxWeek4 As TextBox
    Friend WithEvents LabelWeek1 As Label
    Friend WithEvents LabelWeek2 As Label
    Friend WithEvents LabelWeek3 As Label
    Friend WithEvents LabelWeek4 As Label
    Friend WithEvents LabelWeek5 As Label
    Friend WithEvents ButtonCalculate As Button
    Friend WithEvents ButtonClear As Button
    Friend WithEvents ButtonExit As Button
    Friend WithEvents TextBoxWeek5 As TextBox
    Friend WithEvents LabelAverage As Label
    Friend WithEvents TextBoxAverageCalc As TextBox
End Class
