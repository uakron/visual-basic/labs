﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormKayakBrowser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ListBoxKayakTypes = New System.Windows.Forms.ListBox()
        Me.LabelHeading = New System.Windows.Forms.Label()
        Me.KayaksDataSet = New KayakBrowser.KayaksDataSet()
        Me.KayakTypesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.KayakTypesTableAdapter = New KayakBrowser.KayaksDataSetTableAdapters.KayakTypesTableAdapter()
        Me.TableAdapterManager = New KayakBrowser.KayaksDataSetTableAdapters.TableAdapterManager()
        Me.DescriptionLabel = New System.Windows.Forms.Label()
        CType(Me.KayaksDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KayakTypesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ListBoxKayakTypes
        '
        Me.ListBoxKayakTypes.DataSource = Me.KayakTypesBindingSource
        Me.ListBoxKayakTypes.DisplayMember = "Name"
        Me.ListBoxKayakTypes.FormattingEnabled = True
        Me.ListBoxKayakTypes.Location = New System.Drawing.Point(12, 63)
        Me.ListBoxKayakTypes.Name = "ListBoxKayakTypes"
        Me.ListBoxKayakTypes.Size = New System.Drawing.Size(230, 186)
        Me.ListBoxKayakTypes.TabIndex = 0
        '
        'LabelHeading
        '
        Me.LabelHeading.AutoSize = True
        Me.LabelHeading.Location = New System.Drawing.Point(9, 26)
        Me.LabelHeading.Name = "LabelHeading"
        Me.LabelHeading.Size = New System.Drawing.Size(201, 13)
        Me.LabelHeading.TabIndex = 1
        Me.LabelHeading.Text = "Select a kayak type to view a description"
        '
        'KayaksDataSet
        '
        Me.KayaksDataSet.DataSetName = "KayaksDataSet"
        Me.KayaksDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'KayakTypesBindingSource
        '
        Me.KayakTypesBindingSource.DataMember = "KayakTypes"
        Me.KayakTypesBindingSource.DataSource = Me.KayaksDataSet
        '
        'KayakTypesTableAdapter
        '
        Me.KayakTypesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.KayakTypesTableAdapter = Me.KayakTypesTableAdapter
        Me.TableAdapterManager.UpdateOrder = KayakBrowser.KayaksDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'DescriptionLabel
        '
        Me.DescriptionLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DescriptionLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescriptionLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.KayakTypesBindingSource, "Description", True))
        Me.DescriptionLabel.Location = New System.Drawing.Point(273, 63)
        Me.DescriptionLabel.Name = "DescriptionLabel"
        Me.DescriptionLabel.Size = New System.Drawing.Size(228, 186)
        Me.DescriptionLabel.TabIndex = 3
        Me.DescriptionLabel.Text = "Label1"
        '
        'FormKayakBrowser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(513, 269)
        Me.Controls.Add(Me.DescriptionLabel)
        Me.Controls.Add(Me.LabelHeading)
        Me.Controls.Add(Me.ListBoxKayakTypes)
        Me.Name = "FormKayakBrowser"
        Me.Text = "Kayak Browser"
        CType(Me.KayaksDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KayakTypesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ListBoxKayakTypes As ListBox
    Friend WithEvents LabelHeading As Label
    Friend WithEvents KayaksDataSet As KayaksDataSet
    Friend WithEvents KayakTypesBindingSource As BindingSource
    Friend WithEvents KayakTypesTableAdapter As KayaksDataSetTableAdapters.KayakTypesTableAdapter
    Friend WithEvents TableAdapterManager As KayaksDataSetTableAdapters.TableAdapterManager
    Friend WithEvents DescriptionLabel As Label
End Class
