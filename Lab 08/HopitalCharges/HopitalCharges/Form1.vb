﻿Public Class FormHospitalCharges

    Private intLength As Integer
    Private dblMeds As Double
    Private dblSurgical As Double
    Private dblLab As Double
    Private dblRehab As Double
    Private intChargePerDay As Integer = 350

    Function CalcStayCharges(ByVal intLength As Integer,
                             intChargePerDay As Integer) As Double
        Dim dblStayCharges As Double
        dblStayCharges = intLength * intChargePerDay
        Return dblStayCharges
    End Function

    Function CalcMiscCharges(ByVal dblMeds As Double,
                             ByVal dblSurgical As Double,
                             ByVal dblLab As Double,
                             ByVal dblRehab As Double) As Double
        Dim dblMiscCharges As Double
        dblMiscCharges = dblMeds + dblSurgical + dblLab + dblRehab
        Return dblMiscCharges
    End Function

    Function CalcTotalCharges(ByVal dblStayCharges As Double,
                              ByVal dblMiscCharges As Double) As Double
        Dim dblTotalCharges As Double
        dblTotalCharges = dblStayCharges + dblMiscCharges
        Return dblTotalCharges
    End Function

    Function ValidateInputFields() As Boolean
        If Not Integer.TryParse(TextLength.Text, intLength) Or intLength < 0 Then
            LblStatus.Text = "Please enter a postive integer value."
            TextLength.SelectAll()
            TextLength.Focus()
            Return False
        End If

        If Not Double.TryParse(TextMedication.Text, dblMeds) Or dblMeds < 0 Then
            LblStatus.Text = "Please enter a positive numeric value."
            TextMedication.SelectAll()
            TextMedication.Focus()
            Return False
        End If

        If Not Double.TryParse(TextSurgicalCharges.Text, dblSurgical) Or dblSurgical < 0 Then
            LblStatus.Text = "Please enter a positive numeric value."
            TextSurgicalCharges.SelectAll()
            TextSurgicalCharges.Focus()
            Return False
        End If

        If Not Double.TryParse(TextLabFees.Text, dblLab) Or dblLab < 0 Then
            LblStatus.Text = "Please enter a positive numeric value."
            TextLabFees.SelectAll()
            TextLabFees.Focus()
            Return False
        End If

        If Not Double.TryParse(TextRehab.Text, dblRehab) Or dblRehab < 0 Then
            LblStatus.Text = "Please enter a positive numeric value."
            TextRehab.SelectAll()
            TextRehab.Focus()
            Return False
        End If

        Return True
    End Function

    Private Sub BtnExit_Click(sender As Object, e As EventArgs) Handles BtnExit.Click
        Me.Close()
    End Sub

    Private Sub BtnClear_Click(sender As Object, e As EventArgs) Handles BtnClear.Click
        TextLength.Clear()
        TextMedication.Clear()
        TextSurgicalCharges.Clear()
        TextLabFees.Clear()
        TextRehab.Clear()
        LblStatus.Text = ""
    End Sub

    Private Sub BtnCalculate_Click(sender As Object, e As EventArgs) Handles BtnCalculate.Click
        Dim dblStayCharges, dblMiscCharges, dblTotalCharges As Double

        LblStatus.Text = ""

        If ValidateInputFields() Then
            dblStayCharges = CalcStayCharges(intLength, intChargePerDay)
            dblMiscCharges = CalcMiscCharges(dblMeds, dblSurgical, dblLab, dblRehab)
            dblTotalCharges = CalcTotalCharges(dblStayCharges, dblMiscCharges)
            LblStatus.Text = "Total Cost: " + dblTotalCharges.ToString("c")
        End If
    End Sub
End Class
