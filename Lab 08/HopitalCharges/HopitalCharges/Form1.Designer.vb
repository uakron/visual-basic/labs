﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormHospitalCharges
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextLength = New System.Windows.Forms.TextBox()
        Me.TextMedication = New System.Windows.Forms.TextBox()
        Me.TextSurgicalCharges = New System.Windows.Forms.TextBox()
        Me.TextLabFees = New System.Windows.Forms.TextBox()
        Me.TextRehab = New System.Windows.Forms.TextBox()
        Me.LblStatus = New System.Windows.Forms.Label()
        Me.LblLength = New System.Windows.Forms.Label()
        Me.LblMedication = New System.Windows.Forms.Label()
        Me.LblSurgicalCharges = New System.Windows.Forms.Label()
        Me.LblLabFees = New System.Windows.Forms.Label()
        Me.LblRehab = New System.Windows.Forms.Label()
        Me.BtnCalculate = New System.Windows.Forms.Button()
        Me.BtnClear = New System.Windows.Forms.Button()
        Me.BtnExit = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'TextLength
        '
        Me.TextLength.Location = New System.Drawing.Point(322, 35)
        Me.TextLength.Name = "TextLength"
        Me.TextLength.Size = New System.Drawing.Size(100, 26)
        Me.TextLength.TabIndex = 0
        '
        'TextMedication
        '
        Me.TextMedication.Location = New System.Drawing.Point(322, 81)
        Me.TextMedication.Name = "TextMedication"
        Me.TextMedication.Size = New System.Drawing.Size(100, 26)
        Me.TextMedication.TabIndex = 1
        '
        'TextSurgicalCharges
        '
        Me.TextSurgicalCharges.Location = New System.Drawing.Point(322, 124)
        Me.TextSurgicalCharges.Name = "TextSurgicalCharges"
        Me.TextSurgicalCharges.Size = New System.Drawing.Size(100, 26)
        Me.TextSurgicalCharges.TabIndex = 2
        '
        'TextLabFees
        '
        Me.TextLabFees.Location = New System.Drawing.Point(322, 166)
        Me.TextLabFees.Name = "TextLabFees"
        Me.TextLabFees.Size = New System.Drawing.Size(100, 26)
        Me.TextLabFees.TabIndex = 3
        '
        'TextRehab
        '
        Me.TextRehab.Location = New System.Drawing.Point(322, 206)
        Me.TextRehab.Name = "TextRehab"
        Me.TextRehab.Size = New System.Drawing.Size(100, 26)
        Me.TextRehab.TabIndex = 4
        '
        'LblStatus
        '
        Me.LblStatus.Location = New System.Drawing.Point(108, 251)
        Me.LblStatus.Name = "LblStatus"
        Me.LblStatus.Size = New System.Drawing.Size(314, 37)
        Me.LblStatus.TabIndex = 5
        Me.LblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LblLength
        '
        Me.LblLength.AutoSize = True
        Me.LblLength.Location = New System.Drawing.Point(84, 41)
        Me.LblLength.Name = "LblLength"
        Me.LblLength.Size = New System.Drawing.Size(163, 20)
        Me.LblLength.TabIndex = 6
        Me.LblLength.Text = "Length of Stay (Days)"
        '
        'LblMedication
        '
        Me.LblMedication.AutoSize = True
        Me.LblMedication.Location = New System.Drawing.Point(84, 87)
        Me.LblMedication.Name = "LblMedication"
        Me.LblMedication.Size = New System.Drawing.Size(86, 20)
        Me.LblMedication.TabIndex = 7
        Me.LblMedication.Text = "Medication"
        '
        'LblSurgicalCharges
        '
        Me.LblSurgicalCharges.AutoSize = True
        Me.LblSurgicalCharges.Location = New System.Drawing.Point(84, 130)
        Me.LblSurgicalCharges.Name = "LblSurgicalCharges"
        Me.LblSurgicalCharges.Size = New System.Drawing.Size(130, 20)
        Me.LblSurgicalCharges.TabIndex = 8
        Me.LblSurgicalCharges.Text = "Surgical Charges"
        '
        'LblLabFees
        '
        Me.LblLabFees.AutoSize = True
        Me.LblLabFees.Location = New System.Drawing.Point(84, 172)
        Me.LblLabFees.Name = "LblLabFees"
        Me.LblLabFees.Size = New System.Drawing.Size(76, 20)
        Me.LblLabFees.TabIndex = 9
        Me.LblLabFees.Text = "Lab Fees"
        '
        'LblRehab
        '
        Me.LblRehab.AutoSize = True
        Me.LblRehab.Location = New System.Drawing.Point(84, 212)
        Me.LblRehab.Name = "LblRehab"
        Me.LblRehab.Size = New System.Drawing.Size(118, 20)
        Me.LblRehab.TabIndex = 10
        Me.LblRehab.Text = "Physical Rehab"
        '
        'BtnCalculate
        '
        Me.BtnCalculate.Location = New System.Drawing.Point(33, 302)
        Me.BtnCalculate.Name = "BtnCalculate"
        Me.BtnCalculate.Size = New System.Drawing.Size(128, 58)
        Me.BtnCalculate.TabIndex = 11
        Me.BtnCalculate.Text = "Calculate Charges"
        Me.BtnCalculate.UseVisualStyleBackColor = True
        '
        'BtnClear
        '
        Me.BtnClear.Location = New System.Drawing.Point(201, 302)
        Me.BtnClear.Name = "BtnClear"
        Me.BtnClear.Size = New System.Drawing.Size(128, 58)
        Me.BtnClear.TabIndex = 12
        Me.BtnClear.Text = "Clear Form"
        Me.BtnClear.UseVisualStyleBackColor = True
        '
        'BtnExit
        '
        Me.BtnExit.Location = New System.Drawing.Point(371, 302)
        Me.BtnExit.Name = "BtnExit"
        Me.BtnExit.Size = New System.Drawing.Size(128, 58)
        Me.BtnExit.TabIndex = 13
        Me.BtnExit.Text = "Exit"
        Me.BtnExit.UseVisualStyleBackColor = True
        '
        'FormHospitalCharges
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(531, 394)
        Me.Controls.Add(Me.BtnExit)
        Me.Controls.Add(Me.BtnClear)
        Me.Controls.Add(Me.BtnCalculate)
        Me.Controls.Add(Me.LblRehab)
        Me.Controls.Add(Me.LblLabFees)
        Me.Controls.Add(Me.LblSurgicalCharges)
        Me.Controls.Add(Me.LblMedication)
        Me.Controls.Add(Me.LblLength)
        Me.Controls.Add(Me.LblStatus)
        Me.Controls.Add(Me.TextRehab)
        Me.Controls.Add(Me.TextLabFees)
        Me.Controls.Add(Me.TextSurgicalCharges)
        Me.Controls.Add(Me.TextMedication)
        Me.Controls.Add(Me.TextLength)
        Me.Name = "FormHospitalCharges"
        Me.Text = "Hospital Charges"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextLength As TextBox
    Friend WithEvents TextMedication As TextBox
    Friend WithEvents TextSurgicalCharges As TextBox
    Friend WithEvents TextLabFees As TextBox
    Friend WithEvents TextRehab As TextBox
    Friend WithEvents LblStatus As Label
    Friend WithEvents LblLength As Label
    Friend WithEvents LblMedication As Label
    Friend WithEvents LblSurgicalCharges As Label
    Friend WithEvents LblLabFees As Label
    Friend WithEvents LblRehab As Label
    Friend WithEvents BtnCalculate As Button
    Friend WithEvents BtnClear As Button
    Friend WithEvents BtnExit As Button
End Class
