﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormLatinTranslator
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButtonSinister = New System.Windows.Forms.Button()
        Me.ButtonMedium = New System.Windows.Forms.Button()
        Me.ButtonDexter = New System.Windows.Forms.Button()
        Me.ButtonExit = New System.Windows.Forms.Button()
        Me.LabelTranslation = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ButtonSinister
        '
        Me.ButtonSinister.Location = New System.Drawing.Point(56, 194)
        Me.ButtonSinister.Name = "ButtonSinister"
        Me.ButtonSinister.Size = New System.Drawing.Size(104, 32)
        Me.ButtonSinister.TabIndex = 0
        Me.ButtonSinister.Text = "Sinister"
        Me.ButtonSinister.UseVisualStyleBackColor = True
        '
        'ButtonMedium
        '
        Me.ButtonMedium.Location = New System.Drawing.Point(200, 194)
        Me.ButtonMedium.Name = "ButtonMedium"
        Me.ButtonMedium.Size = New System.Drawing.Size(104, 32)
        Me.ButtonMedium.TabIndex = 1
        Me.ButtonMedium.Text = "Medium"
        Me.ButtonMedium.UseVisualStyleBackColor = True
        '
        'ButtonDexter
        '
        Me.ButtonDexter.Location = New System.Drawing.Point(350, 194)
        Me.ButtonDexter.Name = "ButtonDexter"
        Me.ButtonDexter.Size = New System.Drawing.Size(104, 32)
        Me.ButtonDexter.TabIndex = 2
        Me.ButtonDexter.Text = "Dexter"
        Me.ButtonDexter.UseVisualStyleBackColor = True
        '
        'ButtonExit
        '
        Me.ButtonExit.Location = New System.Drawing.Point(215, 347)
        Me.ButtonExit.Name = "ButtonExit"
        Me.ButtonExit.Size = New System.Drawing.Size(75, 39)
        Me.ButtonExit.TabIndex = 5
        Me.ButtonExit.Text = "Exit"
        Me.ButtonExit.UseVisualStyleBackColor = True
        '
        'LabelTranslation
        '
        Me.LabelTranslation.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTranslation.Location = New System.Drawing.Point(199, 69)
        Me.LabelTranslation.Name = "LabelTranslation"
        Me.LabelTranslation.Size = New System.Drawing.Size(104, 32)
        Me.LabelTranslation.TabIndex = 6
        Me.LabelTranslation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FormLatinTranslator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(504, 420)
        Me.Controls.Add(Me.LabelTranslation)
        Me.Controls.Add(Me.ButtonExit)
        Me.Controls.Add(Me.ButtonDexter)
        Me.Controls.Add(Me.ButtonMedium)
        Me.Controls.Add(Me.ButtonSinister)
        Me.Name = "FormLatinTranslator"
        Me.Text = "Latin Translator"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ButtonSinister As Button
    Friend WithEvents ButtonMedium As Button
    Friend WithEvents ButtonDexter As Button
    Friend WithEvents ButtonExit As Button
    Friend WithEvents LabelTranslation As Label
End Class
