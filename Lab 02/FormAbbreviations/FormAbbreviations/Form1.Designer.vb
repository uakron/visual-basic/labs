﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormAbbrvs
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButtonState1 = New System.Windows.Forms.Button()
        Me.ButtonState2 = New System.Windows.Forms.Button()
        Me.ButtonState3 = New System.Windows.Forms.Button()
        Me.ButtonState4 = New System.Windows.Forms.Button()
        Me.ButtonState5 = New System.Windows.Forms.Button()
        Me.ButtonState6 = New System.Windows.Forms.Button()
        Me.LabelAbbrvs = New System.Windows.Forms.Label()
        Me.ButtonExit = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'ButtonState1
        '
        Me.ButtonState1.Location = New System.Drawing.Point(13, 151)
        Me.ButtonState1.Name = "ButtonState1"
        Me.ButtonState1.Size = New System.Drawing.Size(116, 58)
        Me.ButtonState1.TabIndex = 0
        Me.ButtonState1.Text = "Virginia"
        Me.ButtonState1.UseVisualStyleBackColor = True
        '
        'ButtonState2
        '
        Me.ButtonState2.Location = New System.Drawing.Point(135, 151)
        Me.ButtonState2.Name = "ButtonState2"
        Me.ButtonState2.Size = New System.Drawing.Size(116, 58)
        Me.ButtonState2.TabIndex = 1
        Me.ButtonState2.Text = "North Carolina"
        Me.ButtonState2.UseVisualStyleBackColor = True
        '
        'ButtonState3
        '
        Me.ButtonState3.Location = New System.Drawing.Point(257, 151)
        Me.ButtonState3.Name = "ButtonState3"
        Me.ButtonState3.Size = New System.Drawing.Size(116, 58)
        Me.ButtonState3.TabIndex = 2
        Me.ButtonState3.Text = "South Carolina"
        Me.ButtonState3.UseVisualStyleBackColor = True
        '
        'ButtonState4
        '
        Me.ButtonState4.Location = New System.Drawing.Point(13, 215)
        Me.ButtonState4.Name = "ButtonState4"
        Me.ButtonState4.Size = New System.Drawing.Size(116, 58)
        Me.ButtonState4.TabIndex = 3
        Me.ButtonState4.Text = "Georgia"
        Me.ButtonState4.UseVisualStyleBackColor = True
        '
        'ButtonState5
        '
        Me.ButtonState5.Location = New System.Drawing.Point(135, 215)
        Me.ButtonState5.Name = "ButtonState5"
        Me.ButtonState5.Size = New System.Drawing.Size(116, 58)
        Me.ButtonState5.TabIndex = 4
        Me.ButtonState5.Text = "Alabama"
        Me.ButtonState5.UseVisualStyleBackColor = True
        '
        'ButtonState6
        '
        Me.ButtonState6.Location = New System.Drawing.Point(257, 215)
        Me.ButtonState6.Name = "ButtonState6"
        Me.ButtonState6.Size = New System.Drawing.Size(116, 58)
        Me.ButtonState6.TabIndex = 5
        Me.ButtonState6.Text = "Florida"
        Me.ButtonState6.UseVisualStyleBackColor = True
        '
        'LabelAbbrvs
        '
        Me.LabelAbbrvs.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelAbbrvs.Location = New System.Drawing.Point(114, 33)
        Me.LabelAbbrvs.Name = "LabelAbbrvs"
        Me.LabelAbbrvs.Size = New System.Drawing.Size(158, 57)
        Me.LabelAbbrvs.TabIndex = 6
        Me.LabelAbbrvs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonExit
        '
        Me.ButtonExit.Location = New System.Drawing.Point(286, 311)
        Me.ButtonExit.Name = "ButtonExit"
        Me.ButtonExit.Size = New System.Drawing.Size(87, 35)
        Me.ButtonExit.TabIndex = 7
        Me.ButtonExit.Text = "Exit"
        Me.ButtonExit.UseVisualStyleBackColor = True
        '
        'FormAbbrvs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(385, 358)
        Me.Controls.Add(Me.ButtonExit)
        Me.Controls.Add(Me.LabelAbbrvs)
        Me.Controls.Add(Me.ButtonState6)
        Me.Controls.Add(Me.ButtonState5)
        Me.Controls.Add(Me.ButtonState4)
        Me.Controls.Add(Me.ButtonState3)
        Me.Controls.Add(Me.ButtonState2)
        Me.Controls.Add(Me.ButtonState1)
        Me.Name = "FormAbbrvs"
        Me.Text = "State Abbreviations"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ButtonState1 As Button
    Friend WithEvents ButtonState2 As Button
    Friend WithEvents ButtonState3 As Button
    Friend WithEvents ButtonState4 As Button
    Friend WithEvents ButtonState5 As Button
    Friend WithEvents ButtonState6 As Button
    Friend WithEvents LabelAbbrvs As Label
    Friend WithEvents ButtonExit As Button
End Class
