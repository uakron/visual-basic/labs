﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormVacationsPt2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NameLabel As System.Windows.Forms.Label
        Dim CountryLabel As System.Windows.Forms.Label
        Dim VisitDateLabel As System.Windows.Forms.Label
        Dim DaysLabel As System.Windows.Forms.Label
        Me.VacationsDataSet = New PlanningVacationsPt2.vacationsDataSet()
        Me.LocationsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LocationsTableAdapter = New PlanningVacationsPt2.vacationsDataSetTableAdapters.LocationsTableAdapter()
        Me.TableAdapterManager = New PlanningVacationsPt2.vacationsDataSetTableAdapters.TableAdapterManager()
        Me.NameComboBox = New System.Windows.Forms.ComboBox()
        Me.LocationsBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.CountryTextBox = New System.Windows.Forms.TextBox()
        Me.VisitDateTextBox = New System.Windows.Forms.TextBox()
        Me.DaysTextBox = New System.Windows.Forms.TextBox()
        NameLabel = New System.Windows.Forms.Label()
        CountryLabel = New System.Windows.Forms.Label()
        VisitDateLabel = New System.Windows.Forms.Label()
        DaysLabel = New System.Windows.Forms.Label()
        CType(Me.VacationsDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LocationsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LocationsBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'VacationsDataSet
        '
        Me.VacationsDataSet.DataSetName = "vacationsDataSet"
        Me.VacationsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LocationsBindingSource
        '
        Me.LocationsBindingSource.DataMember = "Locations"
        Me.LocationsBindingSource.DataSource = Me.VacationsDataSet
        '
        'LocationsTableAdapter
        '
        Me.LocationsTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.LocationsTableAdapter = Me.LocationsTableAdapter
        Me.TableAdapterManager.UpdateOrder = PlanningVacationsPt2.vacationsDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'NameLabel
        '
        NameLabel.AutoSize = True
        NameLabel.Location = New System.Drawing.Point(32, 31)
        NameLabel.Name = "NameLabel"
        NameLabel.Size = New System.Drawing.Size(124, 13)
        NameLabel.TabIndex = 0
        NameLabel.Text = "Select a Location Name:"
        '
        'NameComboBox
        '
        Me.NameComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LocationsBindingSource, "Name", True))
        Me.NameComboBox.DataSource = Me.LocationsBindingSource1
        Me.NameComboBox.DisplayMember = "Name"
        Me.NameComboBox.FormattingEnabled = True
        Me.NameComboBox.Location = New System.Drawing.Point(35, 61)
        Me.NameComboBox.Name = "NameComboBox"
        Me.NameComboBox.Size = New System.Drawing.Size(121, 21)
        Me.NameComboBox.TabIndex = 1
        '
        'LocationsBindingSource1
        '
        Me.LocationsBindingSource1.DataMember = "Locations"
        Me.LocationsBindingSource1.DataSource = Me.VacationsDataSet
        '
        'CountryLabel
        '
        CountryLabel.AutoSize = True
        CountryLabel.Location = New System.Drawing.Point(215, 42)
        CountryLabel.Name = "CountryLabel"
        CountryLabel.Size = New System.Drawing.Size(46, 13)
        CountryLabel.TabIndex = 2
        CountryLabel.Text = "Country:"
        '
        'CountryTextBox
        '
        Me.CountryTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LocationsBindingSource1, "Country", True))
        Me.CountryTextBox.Enabled = False
        Me.CountryTextBox.Location = New System.Drawing.Point(267, 42)
        Me.CountryTextBox.Name = "CountryTextBox"
        Me.CountryTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CountryTextBox.TabIndex = 3
        '
        'VisitDateLabel
        '
        VisitDateLabel.AutoSize = True
        VisitDateLabel.Location = New System.Drawing.Point(206, 69)
        VisitDateLabel.Name = "VisitDateLabel"
        VisitDateLabel.Size = New System.Drawing.Size(55, 13)
        VisitDateLabel.TabIndex = 4
        VisitDateLabel.Text = "Visit Date:"
        '
        'VisitDateTextBox
        '
        Me.VisitDateTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LocationsBindingSource1, "VisitDate", True))
        Me.VisitDateTextBox.Enabled = False
        Me.VisitDateTextBox.Location = New System.Drawing.Point(267, 69)
        Me.VisitDateTextBox.Name = "VisitDateTextBox"
        Me.VisitDateTextBox.Size = New System.Drawing.Size(100, 20)
        Me.VisitDateTextBox.TabIndex = 5
        '
        'DaysLabel
        '
        DaysLabel.AutoSize = True
        DaysLabel.Location = New System.Drawing.Point(227, 97)
        DaysLabel.Name = "DaysLabel"
        DaysLabel.Size = New System.Drawing.Size(34, 13)
        DaysLabel.TabIndex = 6
        DaysLabel.Text = "Days:"
        '
        'DaysTextBox
        '
        Me.DaysTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LocationsBindingSource1, "Days", True))
        Me.DaysTextBox.Enabled = False
        Me.DaysTextBox.Location = New System.Drawing.Point(267, 97)
        Me.DaysTextBox.Name = "DaysTextBox"
        Me.DaysTextBox.Size = New System.Drawing.Size(100, 20)
        Me.DaysTextBox.TabIndex = 7
        '
        'FormVacationsPt2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(448, 164)
        Me.Controls.Add(DaysLabel)
        Me.Controls.Add(Me.DaysTextBox)
        Me.Controls.Add(VisitDateLabel)
        Me.Controls.Add(Me.VisitDateTextBox)
        Me.Controls.Add(CountryLabel)
        Me.Controls.Add(Me.CountryTextBox)
        Me.Controls.Add(NameLabel)
        Me.Controls.Add(Me.NameComboBox)
        Me.Name = "FormVacationsPt2"
        Me.Text = "Planning Vacations, Part 2"
        CType(Me.VacationsDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LocationsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LocationsBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents VacationsDataSet As vacationsDataSet
    Friend WithEvents LocationsBindingSource As BindingSource
    Friend WithEvents LocationsTableAdapter As vacationsDataSetTableAdapters.LocationsTableAdapter
    Friend WithEvents TableAdapterManager As vacationsDataSetTableAdapters.TableAdapterManager
    Friend WithEvents NameComboBox As ComboBox
    Friend WithEvents LocationsBindingSource1 As BindingSource
    Friend WithEvents CountryTextBox As TextBox
    Friend WithEvents VisitDateTextBox As TextBox
    Friend WithEvents DaysTextBox As TextBox
End Class
