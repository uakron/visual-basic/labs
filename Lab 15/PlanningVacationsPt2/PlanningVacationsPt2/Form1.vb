﻿Public Class FormVacationsPt2
    Private Sub LocationsBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        Me.LocationsBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.VacationsDataSet)

    End Sub

    Private Sub FormVacationsPt2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'VacationsDataSet.Locations' table. You can move, or remove it, as needed.
        Me.LocationsTableAdapter.Fill(Me.VacationsDataSet.Locations)

    End Sub
End Class
