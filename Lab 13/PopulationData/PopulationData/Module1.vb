﻿Imports System.IO

Module Module1

    Sub Main()
        Const intNumYears As Integer = 40
        Dim intPopulation(intNumYears) As Integer
        Dim strYears() As String = {"1951", "1952", "1953", "1954", "1955", "1956", "1957", "1958", "1959", "1960", "1961", "1962", "1963", "1964", "1965",
                                    "1966", "1967", "1968", "1969", "1970", "1971", "1972", "1973", "1974", "1975", "1976", "1977", "1978", "1979", "1980", "1981",
                                    "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990"}
        Dim inputFile As StreamReader
        Dim intIndex As Integer = 0
        Dim intCount As Integer = 0

        inputFile = File.OpenText("USPopulation.txt")
        Do Until inputFile.EndOfStream Or intIndex >= intPopulation.Length
            intPopulation(intIndex) = CInt(inputFile.ReadLine())
            intIndex = intIndex + 1
        Loop
        inputFile.Close()

        Dim intSum As Integer = 0
        Dim intPop1 As Integer = 0
        Dim intPop2 As Integer = 1
        Dim dblAvgChange As Double
        Do While intPop2 < intPopulation.Length
            intSum += intPopulation(intPop2) - intPopulation(intPop1)
            intPop1 += 1
            intPop2 += 1
        Loop
        dblAvgChange = intSum / intPopulation.Length

        Dim intYearlySum As Integer = 0
        Dim intGreatest As Integer = 0
        Dim strGreatestYear As String
        Dim intLeast As Integer = dblAvgChange
        Dim strLeastYear As String
        intPop1 = 0
        intPop2 = 1
        Do While intPop2 < intPopulation.Length
            intYearlySum = intPopulation(intPop2) - intPopulation(intPop1)
            If intYearlySum > intGreatest Then
                intGreatest = intYearlySum
                strGreatestYear = strYears(intPop2)
            End If
            If intYearlySum < intLeast Then
                intLeast = intYearlySum
                strLeastYear = strYears(intPop2)
            End If
            intPop1 += 1
            intPop2 += 1
        Loop

        Console.WriteLine("The average annual change in population during the time period: " + CStr(dblAvgChange))
        Console.WriteLine("The year with the greatest increase in population during the time period: " + strGreatestYear)
        Console.WriteLine("The year with the least increase in population during the time period: " + strLeastYear)
        Console.Read()
    End Sub

End Module
